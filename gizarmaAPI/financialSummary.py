﻿# -*- coding: utf-8 -*-
from google.appengine.ext import webapp
from google.appengine.ext.webapp.util import run_wsgi_app
from helpTab import HelpTab
from frame import Frame
from ocupations import Ocupation, OcupationGroup, Ocupations
import logging, commonTokens
from apiInterface import ApiInterface
import supplyDemandCalculator
from overview import Overview
import util

GOLD_MULTIPLIER = 100 # TODO move to common, name same as in code, also same for '1200' constant
UNEMPLOYED_INCOME = 3

class FinancialSummary(webapp.RequestHandler):
	def _put(self, s):
		self.response.out.write(s)

	def __initialize(self):
		# all values in raw scale, not yet divided by GOLD_MULTIPLIER
		self._salesIncome = 0 # unbonused, untaxed, without unemployed
		self._unemployedCount = 0
		self._unemployedIncome = 0 # untaxed, unbonused...?
		self._merchantCount = 0
		self._tradeBonusIncome = 0 # untaxed
		self._eliteStudyCost = 0		
		self._cityUpkeep = 0 # unbonused
		self._castellanCount = 0
		self._cityUpkeepBonusSavings = 0
		self._militaryCount = 0
		self._militaryUpkeep = 0
		self._transportCount = 0
		self._transportUpkeep = 0
		self._mapworkerCount = 0
		self._mapworkerUpkeep = 0
		self._scoutCount = 0
		self._scoutUpkeep = 0
		self._luxuryFromTax = 0
	
	# all params in raw scale, not yet divided by GOLD_MULTIPLIER
	def __addCity(self, tax, incom, upkeep, tradeBonus, eliteStudyCost, upkeepBonus, numUnemployed):		
		self._eliteStudyCost += eliteStudyCost
		self._unemployedCount += numUnemployed
		self._unemployedIncome += numUnemployed * UNEMPLOYED_INCOME * GOLD_MULTIPLIER
		
		# Should be like this:
		#        
		# incom = (tradeIncome + numUnemployed * UNEMPLOYED_INCOME * GOLD_MULTIPLIER) * (1 + tradeBonus) * (tax / 100)
		
		# TODO tax = 0?
		untaxedIncome = incom * 100 / tax
		self._luxuryFromTax += untaxedIncome - incom
			
		untaxedIncome = incom * 100 / tax
		unbonusedIncome = untaxedIncome / (1.0 + tradeBonus)
		self._tradeBonusIncome += untaxedIncome - unbonusedIncome
		tradeIncome = unbonusedIncome - UNEMPLOYED_INCOME * GOLD_MULTIPLIER * numUnemployed
		self._salesIncome += tradeIncome
		
		# TODO upkeepBonus = 100?
		unbonusedUpkeep = upkeep * 100 / (100 - upkeepBonus)
		self._cityUpkeepBonusSavings += unbonusedUpkeep - upkeep
		self._cityUpkeep += unbonusedUpkeep
	
	def __addUnit(self, ocupation):
		if ocupation.ocupationGroup == OcupationGroup.MILITARY:
			self._militaryCount += 1
			self._militaryUpkeep += ocupation.cost * GOLD_MULTIPLIER
		elif ocupation.ocupationGroup == OcupationGroup.MAPWORKER:
			self._mapworkerCount += 1
			self._mapworkerUpkeep += ocupation.cost * GOLD_MULTIPLIER
		elif ocupation.ocupationGroup == OcupationGroup.SCOUT:
			self._scoutCount += 1
			self._scoutUpkeep += ocupation.cost * GOLD_MULTIPLIER
		elif ocupation.ocupationGroup == OcupationGroup.TRANSPORT:
			self._transportCount += 1
			self._transportUpkeep += ocupation.cost * GOLD_MULTIPLIER	
		elif ocupation.ocupationGroup in [OcupationGroup.ELITE, OcupationGroup.KING]:
			if ocupation == Ocupations.Merchant:
				self._merchantCount += 1
			elif ocupation in [Ocupations.Castellan, Ocupations.Lord]:
				self._castellanCount += 1
	
	def __output(self, overviewData):
		self.__initialize()
		
		cities = overviewData['cities']
		tax = int(overviewData['kingdom']['tax'])
		
		
		ov = Overview() # helper instance; TODO refactor
		for city in cities.values():
			incom = int(city['incom'])
			upkeep = int(city['upkeep'])
			tradePower = int(city['tradeBonus'])			
			(numCitizens, _, _) = ov.getNumCitizensNumUnemployedAndEliteListInCity(overviewData, city)
			tradeBonus = util.getTradeBonus(tradePower, numCitizens)
			eliteStudyCost = int(city['eliteStudyCost'])
			upkeepBonus = int(city['upkeepBonus'])
			numUnemployed = len(city['units']['unemployed'])
			self.__addCity(tax, incom, upkeep, tradeBonus, eliteStudyCost, upkeepBonus, numUnemployed)
		
		for unit in overviewData['units'].values():
			ocupationId = unit['idOccupation']
			ocupation = Ocupations.get(int(ocupationId))
			self.__addUnit(ocupation)
		
		totalIncome = self._salesIncome + self._unemployedIncome + self._tradeBonusIncome - self._luxuryFromTax
		totalExpenses = self._cityUpkeep + self._militaryUpkeep + self._transportUpkeep + self._mapworkerUpkeep + self._scoutUpkeep + self._eliteStudyCost - self._cityUpkeepBonusSavings		

		supplyDemandInKingdom = supplyDemandCalculator.getSupplyDemandInKingdom(overviewData)
		kingIncome = self._salesIncome # need to calculate king's income as salesIncome minus all partial sales incomes

		tIncome = HelpTab(self._put, width=688)
		tIncome.setHeaders(['&nbsp;', '&nbsp;Przychody&nbsp;', '&nbsp;'])
		for (good, sds) in supplyDemandInKingdom.totals.sold.iteritems():
			numSold = supplyDemandInKingdom.totals.sold.get(good)
			profit = numSold * good.price
			if profit > 0:
				tIncome.addRow([
					u'Sprzedaż <img src="/img/goods/%s.png" title="%s">:' % (good.code, good.goodname), 
					u'%s x %s<img width="15px" src="/img/goods/36.png"> =' % (numSold / GOLD_MULTIPLIER, good.price), 
					u"<div style='color: #1B5F00'>%s</div>" % (profit / GOLD_MULTIPLIER)
				])
				kingIncome = kingIncome - profit
		tIncome.addRow([u'Dochód króla:', '&nbsp;', "<div style='color: #1B5F00'>%0.0f</div>" % (kingIncome / GOLD_MULTIPLIER)])
		tIncome.addRow([u'Bezrobotni:', '%s' % self._unemployedCount, 
					"<div style='color: #1B5F00'>%s</div>" % (self._unemployedIncome / GOLD_MULTIPLIER)])
		tIncome.addRow([u'Kupcy:', '%s' % self._merchantCount, 
					"<div style='color: #1B5F00'>%0.0f</div>" % (self._tradeBonusIncome / GOLD_MULTIPLIER)])
		tIncome.addRow([u'Wydatki na zadowolenie (z podatków)', '&nbsp;', "<div style='color: #87020E'>-%s</div>" % (self._luxuryFromTax / GOLD_MULTIPLIER)])
		tIncome.addRow(['&nbsp;', '&nbsp;', '&nbsp;'])
		tIncome.addRow(['Suma:', '&nbsp;', "<div style='color: #1B5F00'>%0.0f</div>" % (totalIncome / GOLD_MULTIPLIER)])
		tIncome.printHTML()
		
		self._put('<br><br>')
		
		tExpenses = HelpTab(self._put, width=688)
		tExpenses.setHeaders(['&nbsp;', '&nbsp;Wydatki&nbsp;', '&nbsp;'])
		tExpenses.addRow([u'Utrzymanie miast:', '&nbsp;', "<div style='color: #87020E'>-%s</div>" % (self._cityUpkeep / GOLD_MULTIPLIER)])
		tExpenses.addRow([u'Oszczędności dzięki władcy i kasztelanom:', '%s jedn.' % self._castellanCount, 
					"<div style='color: #1B5F00'>%s</div>" % (self._cityUpkeepBonusSavings / GOLD_MULTIPLIER)])
		tExpenses.addRow([u'Wojsko:', '%s jedn.' % self._militaryCount, 
					"<div style='color: #87020E'>-%s</div>" % (self._militaryUpkeep / GOLD_MULTIPLIER)])
		tExpenses.addRow([u'Tragarze i karawany:', '%s jedn.' % self._transportCount, 
					"<div style='color: #87020E'>-%s</div>" % (self._transportUpkeep / GOLD_MULTIPLIER)])
		tExpenses.addRow([u'Osadnicy i budowniczowie:', '%s jedn.' % self._mapworkerCount, 
					"<div style='color: #87020E'>-%s</div>" % (self._mapworkerUpkeep / GOLD_MULTIPLIER)])
		tExpenses.addRow([u'Pionierzy i zwiadowcy:', '%s jedn.' % (self._scoutCount), 
					"<div style='color: #87020E'>-%s</div>" % (self._scoutUpkeep / GOLD_MULTIPLIER)])
		tExpenses.addRow([u'Edukacja elity:', '&nbsp;', "<div style='color: #87020E'>-%s</div>" % (self._eliteStudyCost / GOLD_MULTIPLIER)])
		tExpenses.addRow(['&nbsp;', '&nbsp;', '&nbsp;'])
		tExpenses.addRow(['Suma:', '&nbsp;', "<div style='color: #87020E'>-%s</div>" % (totalExpenses / GOLD_MULTIPLIER)])
		tExpenses.printHTML()
		
		balance = totalIncome - totalExpenses
		balanceString = ("<span style='font-family: arial;'>-</span>%0.0f" % (-balance / GOLD_MULTIPLIER)) if balance < 0 else '+%0.0f' % (balance  / GOLD_MULTIPLIER)
		self._put("<b>Bilans: &nbsp;&nbsp; <span style='color: %s'>%s</span></b><br/>" % ('#87020E' if balance < 0 else '#1B5F00', balanceString))
		cash = int(overviewData['kingdom']['gold']) / 1200 # TODO '1200' should be a constant
		self._put("<b>Kasa: %s<img width='15px' src='/img/goods/36.png'/><br/>" % cash)
		self._put('<b>Podatki</b>: %s%%<br/>' % overviewData['kingdom']['tax'])
	
	def get(self):
		self.response.headers['Content-Type'] = 'text/html'
		self._put('<html>')
		commonTokens.writeHeader(self._put, "Podsumowanie finansowe")
		
		self._put('<body><div style="width:1000px; margin: auto">')
		fr = Frame(self._put)
		fr.open()
		
		serverId = self.request.get('serverId', default_value='main')
		api = ApiInterface(serverId)
		
		gameId = self.request.get('gameId')
		kingdomId = self.request.get('kingdom')
		overviewData = api.getOverviewData(gameId, kingdomId, True)
		self._put('<b>Podsumowanie finansowe</b> - ' + commonTokens.countryNameWithFlag(api, overviewData['kingdom']['flag'], overviewData['kingdom']['name']))
		self._put(commonTokens.backButton('/overview?gameId=%s&kingdom=%s' % (gameId, kingdomId), api.serverId))
		self.__output(overviewData)
	
		fr.close()
		self._put('</div>')
		self._put('</body></html>')


financialApplication = webapp.WSGIApplication([('/financialSummary.*', FinancialSummary)], debug=True)

def main():
	logging.getLogger().setLevel(logging.ERROR)
	run_wsgi_app(financialApplication)

if __name__ == "__main__":
	main()

