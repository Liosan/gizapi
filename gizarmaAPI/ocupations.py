﻿# -*- coding: utf-8 -*-

from enum import Enum

class OcupationGroup:
	UNEMPLOYED=0
	PEASANT=1
	TOWNSMAN=2
	ELITE=3
	MAPWORKER=4
	MILITARY=5
	TRANSPORT=6
	SCOUT=7
	KING=8
	

class Ocupation:
	def __init__(self, code, unitname, ocupationGroup, image, cost, milStrength=0, milStrengthPerLvl=0, milSpecialty=0, milMinLevel=0):
		self.code = code
		self.unitname = unitname
		self.ocupationGroup = ocupationGroup
		self.image = image
		self.cost = cost
		self.milStrength = milStrength
		self.milStrengthPerLvl = milStrengthPerLvl
		self.milSpecialty = milSpecialty
		self.milMinLevel = milMinLevel

class Ocupations(Enum):
	Unemployed=Ocupation(0, "Bezrobotny", OcupationGroup.UNEMPLOYED, "specialty-0.png", 0)
	
	Farmer=Ocupation(1, "Rolnik", OcupationGroup.PEASANT, "specialty-1.png", 0)
	Woodcutter=Ocupation(2, "Drwal", OcupationGroup.PEASANT, "specialty-2.png", 0)
	Claydigger=Ocupation(3, "Kopacz gliny", OcupationGroup.PEASANT, "specialty-3.png", 0)
	OreMiner=Ocupation(4, u"Górnik rudy żelaza", OcupationGroup.PEASANT, "specialty-4.png", 0)
	Hunter=Ocupation(5, u"Myśliwy", OcupationGroup.PEASANT, "specialty-5.png", 0)
	GrapeFarmer=Ocupation(6, u"Hodowca winorośli", OcupationGroup.PEASANT, "specialty-6.png", 0)
	SaltGatherer=Ocupation(7, "Zbieracz soli", OcupationGroup.PEASANT, "specialty-7.png", 0)
	GemMiner=Ocupation(8, u"Górnik kamieni szlachetnych", OcupationGroup.PEASANT, "default.png", 0)
	Fisherman=Ocupation(9, "Rybak", OcupationGroup.PEASANT, "specialty-9.png", 0)
	HopsFarmer=Ocupation(10, "Hodowca chmielu", OcupationGroup.PEASANT, "specialty-10.png", 0)
	ElephantHunter=Ocupation(11, u"Łowca słoni", OcupationGroup.PEASANT, "default.png", 0)
	
	Carpenter=Ocupation(20, u"Cieśla", OcupationGroup.TOWNSMAN, "specialty-20.png", 0)
	BowyerShort=Ocupation(21, u"Łuczarz (łuki krótkie)", OcupationGroup.TOWNSMAN, "default.png", 0)
	BowyerLong=Ocupation(22, u"Łuczarz (łuki długie)", OcupationGroup.TOWNSMAN, "default.png", 0)
	Brickmaker=Ocupation(23, "Ceglarz", OcupationGroup.TOWNSMAN, "specialty-22.png", 0)
	Metallurgist=Ocupation(24, "Hutnik", OcupationGroup.TOWNSMAN, "specialty-23.png", 0)
	BlacksmithTools=Ocupation(25, u"Kowal (narzędzia)", OcupationGroup.TOWNSMAN, "specialty-24.png", 0)
	BlacksmithScissors=Ocupation(26, u"Kowal (nożyce)", OcupationGroup.TOWNSMAN, "specialty-24.png", 0)
	WeaponsmithPikes=Ocupation(27, u"Płatnerz (piki)", OcupationGroup.TOWNSMAN, "default.png", 0)
	WeaponsmithSwords=Ocupation(28, u"Płatnerz (miecze)", OcupationGroup.TOWNSMAN, "default.png", 0)
	Tailor=Ocupation(29, "Szwacz", OcupationGroup.TOWNSMAN, "specialty-27.png", 0)
	Potter=Ocupation(30, "Garncarz", OcupationGroup.TOWNSMAN, "specialty-28.png", 0)
	Winemaker=Ocupation(31, u"Wytwórca wina", OcupationGroup.TOWNSMAN, "specialty-29.png", 0)
	HorseBreeder=Ocupation(32, "Hodowca koni", OcupationGroup.TOWNSMAN, "default.png", 0)
	Brewer=Ocupation(33, "Piwowar", OcupationGroup.TOWNSMAN, "specialty-32.png", 0)
	Pianomaker=Ocupation(34, u"Lutnik", OcupationGroup.TOWNSMAN, "default.png", 0)
	HorseCatcher=Ocupation(35, "Łowca dzikich koni", OcupationGroup.TOWNSMAN, "default.png", 0)
	CarpenterHardwood=Ocupation(36, "Cieśla (drewno szlachetne)", OcupationGroup.TOWNSMAN, "specialty-20.png", 0)
	
	SiegeEngineer=Ocupation(41, "Wytwórca machin", OcupationGroup.TOWNSMAN, "default.png", 0)
	
	Builder=Ocupation(100, "Budowniczy", OcupationGroup.TOWNSMAN, "specialty-30.png", 0)
	
	Castellan=Ocupation(60, "Kasztelan", OcupationGroup.ELITE, "specialty-71.png", 0)
	Merchant=Ocupation(61, "Kupiec", OcupationGroup.ELITE, "specialty-72.png", 0)
	Priest=Ocupation(62, u"Kapłan", OcupationGroup.ELITE, "specialty-73.png", 0)
	
	Archer=Ocupation(70, u"Łucznik", OcupationGroup.MILITARY, "occupation-70.png", 5, milStrength=30, milStrengthPerLvl=3, milSpecialty=50)
	Longbowman=Ocupation(71, u"Łucznik angielski", OcupationGroup.MILITARY, "occupation-71.png", 6, milStrength=60, milStrengthPerLvl=6, milSpecialty=50, milMinLevel=5)
	Pikeman=Ocupation(73, "Pikinier", OcupationGroup.MILITARY, "occupation-73.png", 6, milStrength=30, milStrengthPerLvl=3, milSpecialty=51)
	Swordsman=Ocupation(74, "Miecznik", OcupationGroup.MILITARY, "default.png", 8, milStrength=60, milStrengthPerLvl=6, milSpecialty=51, milMinLevel=5)
	LightCavalryman=Ocupation(76, "Lekki kawalerzysta", OcupationGroup.MILITARY, "occupation-76.png", 8, milStrength=50, milStrengthPerLvl=4, milSpecialty=52)
	Knight=Ocupation(77, "Rycerz", OcupationGroup.MILITARY, "occupation-77.png", 10, milStrength=100, milStrengthPerLvl=12, milSpecialty=52, milMinLevel=6	)
	Warrior=Ocupation(78, "Wojownik", OcupationGroup.MILITARY, "occupation-78.png", 4, milStrength=20, milStrengthPerLvl=2, milSpecialty=51)
	
	Pioneer=Ocupation(72, "Pionier", OcupationGroup.SCOUT, "occupation-72.png", 4)
	Scout=Ocupation(75, "Zwiadowca", OcupationGroup.SCOUT, "occupation-75.png", 6)

	Porter=Ocupation(90, "Tragarz", OcupationGroup.TRANSPORT, "occupation-90.png", 3)
	Caravan=Ocupation(91, "Karawana", OcupationGroup.TRANSPORT, "occupation-91.png", 5)
	
	Settler=Ocupation(101, "Osadnik", OcupationGroup.MAPWORKER, "occupation-101.png", 3)
	Worker=Ocupation(102, "Robotnik", OcupationGroup.MAPWORKER, "occupation-102.png", 5)
	
	Lord=Ocupation(110, u"Władca", OcupationGroup.KING, "specialty-90.png", 0)
	
	TreasureWagon=Ocupation(120, "Wóz ze skarbami", OcupationGroup.TRANSPORT, "occupation-120.png", 3)

