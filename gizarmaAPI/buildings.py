﻿# -*- coding: utf-8 -*-

from enum import Enum

MAIN_IMG = "/img/large/buildings.png"
MAIN_IMG_HEIGHT = 80
OFFSET_MULTIPLIER = 90
INVALID_OFFSET = -1
PLACEHOLDER_OFFSET = 0

class BuildingType:
	def __init__(self, code, btName):
		self.code = code
		self.btName = btName

class BuildingTypes(Enum):
	BASIC=BuildingType(0, "Podstawowe")
	WORKSHOP=BuildingType(1, "Warsztaty")
	ADMINISTRATIVE=BuildingType(2, "Administracyjne")
	HABITABLE=BuildingType(3, "Mieszkalne")
	MERCANTILE=BuildingType(4, "Handlowe")
	STORAGE=BuildingType(5, "Magazyny")
	SACRAL=BuildingType(6, "Sakralne")
	SCHOOL=BuildingType(7, "Szkolne")
	SEAFARING=BuildingType(8, "Morskie")


class Building:
	def __init__(self, buildingname, code, buildingType, img, imgOffset, materials):
		self.buildingname = buildingname
		self.code = code
		self.buildingType = buildingType
		self.img = img
		self.imgOffset = imgOffset
		self.materials = materials # materials are a with any of the letters WBT - meaning wood/bricks/tools are needed. can be empty
		assert img == MAIN_IMG or imgOffset == INVALID_OFFSET, "Malconfigured building image for building " + code + " (" + buildingname + ")"
		
	def hasOffset(self):
		return self.imgOffset != INVALID_OFFSET

class Buildings(Enum):
	CraftsmanHome=Building(u"Dom rzemieślnika", 10, BuildingTypes.BASIC, MAIN_IMG, 40, "")
	WoodenHut=Building(u"Drewniana chata", 50, BuildingTypes.BASIC, MAIN_IMG, 36, "")
	SmallWarehouse=Building(u"Mały magazyn", 70, BuildingTypes.BASIC, MAIN_IMG, 43, "")
	
	CarpenterHome=Building(u"Dom cieśli", 11, BuildingTypes.WORKSHOP, MAIN_IMG, 56, "WBT")
	CarpenterWorkshop=Building(u"Warsztat cieśli", 12, BuildingTypes.WORKSHOP, MAIN_IMG, 91, "WBT")
	SmithHome=Building(u"Dom kowala", 13, BuildingTypes.WORKSHOP, MAIN_IMG, 20, "WBT")
	Smithery=Building(u"Kuźnia", 14, BuildingTypes.WORKSHOP, MAIN_IMG, 89, "WBT")
	SmelterSmall=Building(u"Dymarki", 15, BuildingTypes.WORKSHOP, MAIN_IMG, 22, "WBT")
	SmelterLarge=Building(u"Piec hutniczy", 16, BuildingTypes.WORKSHOP, MAIN_IMG, 86, "WBT")
	BrickerySmall=Building(u"Mały piec ceglarski", 17, BuildingTypes.WORKSHOP, MAIN_IMG, 61, "WBT")
	BrickeryLarge=Building(u"Cegielnia", 18, BuildingTypes.WORKSHOP, MAIN_IMG, 78, "WBT")
	WinemakerHome=Building(u"Dom wytwórcy trunków", 19, BuildingTypes.WORKSHOP, MAIN_IMG, 65, "WB")
	Winery=Building(u"Wytwórnia trunków", 20, BuildingTypes.WORKSHOP, MAIN_IMG, 93, "WBT")
	ClothierHome=Building(u"Dom szwacza", 21, BuildingTypes.WORKSHOP, MAIN_IMG, 26, "WB")
	Clothery=Building(u"Wytwórnia ubrań", 22, BuildingTypes.WORKSHOP, MAIN_IMG, 87, "WBT")
	PotterHome=Building(u"Dom garncarza", 23, BuildingTypes.WORKSHOP, MAIN_IMG, 24, "WB")
	Pottery=Building(u"Piec garncarski", 24, BuildingTypes.WORKSHOP, MAIN_IMG, 34, "WBT")
	Corral=Building(u"Zagroda", 25, BuildingTypes.WORKSHOP, MAIN_IMG, 72, "WB")
	Stable=Building(u"Stajnia", 26, BuildingTypes.WORKSHOP, MAIN_IMG, 77, "WB")
	SiegemakerHome=Building(u"Dom wytwórcy machin", 30, BuildingTypes.WORKSHOP, MAIN_IMG, 63, "WBT")
	SiegeWorkshop=Building(u"Warsztat artyleryjski", 31, BuildingTypes.WORKSHOP, MAIN_IMG, PLACEHOLDER_OFFSET, "WBT")
	PianoWorkshop=Building(u"Warsztat lutnika", 29, BuildingTypes.WORKSHOP, MAIN_IMG, 96, "WBT")
	
	VillageHall=Building(u"Dom rady osady", 40, BuildingTypes.ADMINISTRATIVE, MAIN_IMG, 12, "WB")
	CityHall=Building(u"Ratusz", 41, BuildingTypes.ADMINISTRATIVE, MAIN_IMG, 17, "WB")
	Palace=Building(u"Pałac", 42, BuildingTypes.ADMINISTRATIVE, MAIN_IMG, PLACEHOLDER_OFFSET, "WB")
	
	WoodenHouse=Building(u"Drewniany dom", 51, BuildingTypes.HABITABLE, MAIN_IMG, 8, "W")
	BrickedHouse=Building(u"Dom murowany", 52, BuildingTypes.HABITABLE, MAIN_IMG, 82, "WB")
	LargeHouse=Building(u"Kamienica", 53, BuildingTypes.HABITABLE, MAIN_IMG, 13, "WB")

	Marketplace=Building(u"Plac targowy", 60, BuildingTypes.MERCANTILE, MAIN_IMG, 31, "W")
	Market=Building(u"Rynek", 61, BuildingTypes.MERCANTILE, MAIN_IMG, 16, "WB")

	WoodenWarehouse=Building(u"Drewniany magazyn", 71, BuildingTypes.STORAGE, MAIN_IMG, 69, "W")
	BrickedWarehouse=Building(u"Murowany magazyn", 72, BuildingTypes.STORAGE, MAIN_IMG, 67, "WB")
	LargeWarehouse=Building(u"Duży magazyn", 73, BuildingTypes.STORAGE, MAIN_IMG, 74, "WB")

	Shrine=Building(u"Kapliczka", 80, BuildingTypes.SACRAL, MAIN_IMG, 54, "W")
	Chapel=Building(u"Świątynia", 81, BuildingTypes.SACRAL, MAIN_IMG, 18, "WB")
	Church=Building(u"Wielka świątynia", 82, BuildingTypes.SACRAL, "/img/buildings/church1_0.png", INVALID_OFFSET, "WB")
	Cathedral=Building(u"Katedra", 83, BuildingTypes.SACRAL, "/img/buildings/cathedral0.png", INVALID_OFFSET, "WB")

	School=Building(u"Szkoła", 90, BuildingTypes.SCHOOL, MAIN_IMG, 60, "WB")
	Academy=Building(u"Akademia", 91, BuildingTypes.SCHOOL, MAIN_IMG, 80, "WB")
	University=Building(u"Uniwersytet", 92, BuildingTypes.SCHOOL, "/img/buildings/university0.png", INVALID_OFFSET, "WB")

	SmallLighthouse=Building(u"Mała latarnia morska", 100, BuildingTypes.SEAFARING, "/img/buildings/lighthouse1.png", INVALID_OFFSET, "WB")
	LargeLighthouse=Building(u"Latarnia morska", 101, BuildingTypes.SEAFARING, "/img/buildings/lighthouse2.png", INVALID_OFFSET, "WB")
	Harbor=Building(u"Przystań", 105, BuildingTypes.SEAFARING, MAIN_IMG, 50, "WB")
	Port=Building(u"Port", 106, BuildingTypes.SEAFARING, MAIN_IMG, PLACEHOLDER_OFFSET, "WB")
	SmallShipyard=Building(u"Mała stocznia", 110, BuildingTypes.SEAFARING, MAIN_IMG, PLACEHOLDER_OFFSET, "WBT")
	Shipyard=Building(u"Stocznia", 111, BuildingTypes.SEAFARING, MAIN_IMG, PLACEHOLDER_OFFSET, "WBT")


