﻿# -*- coding: utf-8 -*-

from enum import Enum

class GoodsGroup:
	RAW=0
	CONSTRUCTION=1
	MILITARY=2
	TRADE=3
	LUXURY=4

class Good:
	def __init__(self, code, goodname, price, demand, goodsGroup):
		self.code = code
		self.goodname = goodname
		self.price = price
		self.demand = demand
		self.goodsGroup = goodsGroup

class Goods(Enum):
	Food=Good(0, "Jedzenie", 0, 0, GoodsGroup.RAW) # Food as a Raw is a bit fishy
	Wood=Good(1, "Drewno", 0, 0, GoodsGroup.RAW)
	Boards=Good(2, "Deski", 0, 0, GoodsGroup.CONSTRUCTION)
	Clay=Good(3, "Glina", 0, 0, GoodsGroup.RAW)
	Bricks=Good(4, u"Cegły", 0, 0, GoodsGroup.CONSTRUCTION)
	Ore=Good(5, "Ruda", 0, 0, GoodsGroup.RAW)
	Iron=Good(6, u"Żelazo", 0, 0, GoodsGroup.RAW)
	Tools=Good(7, u"Narzędzia", 0, 0, GoodsGroup.CONSTRUCTION)
	Furs=Good(8, "Futra", 0, 0, GoodsGroup.RAW)
	Clothes=Good(9, "Ubrania", 7, 0.67, GoodsGroup.TRADE)
	Ceramics=Good(10, "Garnki", 7, 0.67, GoodsGroup.TRADE)
	Grapes=Good(11, "Winogrona", 0, 0, GoodsGroup.RAW)
	Drinks=Good(12, "Trunki", 15, 0.34, GoodsGroup.TRADE)
	Salt=Good(13, u"Sól", 32, 0.18, GoodsGroup.LUXURY)
	Scissors=Good(14, u"Nożyce", 24, 0.25, GoodsGroup.TRADE)
	Ambregris=Good(15, "Ambra", 50, 0.12, GoodsGroup.LUXURY)
	Gems=Good(16, "Kamienie szlachetne", 50, 0.12, GoodsGroup.LUXURY)
	Horses=Good(17, "Konie", 0, 0, GoodsGroup.MILITARY) # hm. Weird. Whatever, horses are pretty military
	Shortbows=Good(18, u"Łuki krótkie", 0, 0, GoodsGroup.MILITARY)
	Longbows=Good(19, u"Łuki długie", 0, 0, GoodsGroup.MILITARY)
	Pikes=Good(20, "Piki", 0, 0, GoodsGroup.MILITARY)
	Swords=Good(21, "Miecze", 0, 0, GoodsGroup.MILITARY)
	Hops=Good(22, "Chmiel", 0, 0, GoodsGroup.RAW)
	Hardwood=Good(23, "Drewno szlachetne", 0, 0, GoodsGroup.RAW)
	Ivory=Good(24, u"Kość słoniowa", 50, 0.12, GoodsGroup.LUXURY)
	Pianos=Good(25, "Harfy", 80, 0.08, GoodsGroup.LUXURY)


