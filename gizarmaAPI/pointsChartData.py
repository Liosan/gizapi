﻿# -*- coding: utf-8 -*-

import extOfc
from extOfc_element import Line
from google.appengine.ext import webapp, db
from google.appengine.ext.webapp.util import run_wsgi_app
from schema import RankingPointsSampleList
from collections import defaultdict
import logging
from apiInterface import ApiInterface

class PointsChartData(webapp.RequestHandler):	
	_colorsTable = ["#FF3333", "#66FF33", "#660033", "#3366CC", "#9966CC", "#CC66CC", "#FF66CC", "#000000", "#9669FE", "#996600"]
	
	def get(self):		
		serverId = self.request.get('serverId', default_value='main')
		requestedKingdomId = self.request.get('kingdom', default_value='')
		api = ApiInterface(serverId)
		gameId = self.request.get('gameId')

		# get stored values
		q = RankingPointsSampleList.all()
		q.filter("gameId = ", int(gameId))
		q.filter("serverId = ", serverId) 
		
		# calculate min-max on y-axis
		maxValue = 50
		values = defaultdict(list)
		for entry in q.run():
			logging.debug ("Kingdom %s found in historical data" % entry.kingdomId)
			values[entry.kingdomId] += entry.pointsList
			currentMax = max(entry.pointsList)
			if maxValue < currentMax:
				maxValue = currentMax
		
		# get current values		
		fetchedHighScores = api.getRankingData(gameId)
		currentScoreDict = {}
		for entry in fetchedHighScores:
			kingdomId = int(entry['id'])
			points = int(entry['points'])
			name = entry['name']
			logging.debug ("Kingdom %s found in current data" % kingdomId)
			currentScoreDict[kingdomId] = (points, name)

		# calculate maxLen
		maxLen = 0
		for (kingdomId, (points, name)) in currentScoreDict.items():
			pointsList = values[kingdomId]
			if len(pointsList) > maxLen:
				maxLen = len(pointsList)
				
		# format		
		self.chart = extOfc.open_flash_chart(title='')
		colorIdx = 0
		for (kingdomId, (points, name)) in currentScoreDict.items():
			if requestedKingdomId != '' and int(requestedKingdomId) != kingdomId:
				continue
			pointsList = values[kingdomId]
			# prepend some 0's, so that all lists are equal in length
			pointsList = [0] * (maxLen - len(pointsList)) + pointsList
			pointsList.append(points)
			color = self._colorsTable[colorIdx % len(self._colorsTable)]
			element = Line(colour = color, text = name, values = pointsList)
			element['tip'] = '#key#: #val#'
			self.chart.add_element(element)	
			colorIdx += 1		
		
		# reply
		self.chart.set_y_axis(max = maxValue * 1.2, steps = maxValue / 20)		
		self.response.out.write(self.chart.encode())
		
pointsChartApplication = webapp.WSGIApplication([('/pointsChartData', PointsChartData)], debug=True)

def main():
	logging.getLogger().setLevel(logging.DEBUG)
	run_wsgi_app(pointsChartApplication)
	
if __name__ == "__main__":
	 main()
