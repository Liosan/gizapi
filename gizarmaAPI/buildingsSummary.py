﻿# -*- coding: utf-8 -*-

from google.appengine.ext import webapp
from google.appengine.ext.webapp.util import run_wsgi_app
from helpTab import HelpTab
from frame import Frame
import commonTokens, logging
from apiInterface import ApiInterface
from buildings import Buildings, MAIN_IMG_HEIGHT, OFFSET_MULTIPLIER, BuildingTypes

class BuildingsSummary(webapp.RequestHandler):	
	def _put(self, s):
		self.response.out.write(s)

	def output(self, overviewData):		
		buildings = [0] * 120 # TODO implement Enum.maxCode
		for city in overviewData['cities'].values():
			buildArr = city['buildings']
			for r in range(len(buildArr)):
				buildingId = int(buildArr[r]['id'])
				state = int(buildArr[r]['state'])
				if state == 0: # constructed
					buildings[buildingId] += 1
		
		spriteString = "<div style='background-image: url(\"%s\"); background-position: -%spx 0px; background-repeat: no-repeat;" \
			" width: %spx; height: " + str(MAIN_IMG_HEIGHT) + "px; float: left'>&nbsp;</div>" \
			"<div style='float: left;'>%s</div>";
		imgString = "<img src='%s'>&nbsp;%s"
		for buildingType in BuildingTypes:
			self._put('<p class="highlight">%s</p>' % buildingType.btName)
			t = HelpTab(self._put, width=688)
			t.setWidths([200, 29, 200, 28, 200, 29])
			row = []
			for building in Buildings:
				if building.buildingType == buildingType:
					if building.hasOffset():
						row.append(spriteString % (building.img, building.imgOffset * OFFSET_MULTIPLIER, OFFSET_MULTIPLIER, building.buildingname))
					else:
						row.append(imgString % (building.img, building.buildingname))
					row.append("%s" % buildings[building.code])
					if len(row) == 4:
						t.addRow(row)
						row = []
			if len(row) > 0:
				while len(row) < 4:
					row.append("")
				t.addRow(row)
		
			t.printHTML()
			
	def get(self):
		self.response.headers['Content-Type'] = 'text/html'
		self._put('<html>')
		commonTokens.writeHeader(self._put, "Lista budynków")
		
		self._put('<body><div style="width:1000px; margin: auto">')
		fr = Frame(self._put)
		fr.open()
		
		serverId = self.request.get('serverId', default_value='main')
		api = ApiInterface(serverId)
		
		gameId = self.request.get('gameId')
		kingdomId = self.request.get('kingdom')
		overviewData = api.getOverviewData(gameId, kingdomId, False)

		self._put('<b>Lista budynkow</b> - ' + commonTokens.countryNameWithFlag(api, overviewData['kingdom']['flag'], overviewData['kingdom']['name']))
		self._put(commonTokens.backButton('/overview?gameId=%s&kingdom=%s' % (gameId, kingdomId), api.serverId))
		self._put('<br><br>')

		self.output(overviewData)
	
		fr.close()
		self._put('</div>')
		self._put('</body></html>')


inventoryApplication = webapp.WSGIApplication([('/buildingsSummary.*', BuildingsSummary)], debug=True)

def main():
	logging.getLogger().setLevel(logging.ERROR)
	run_wsgi_app(inventoryApplication)

if __name__ == "__main__":
	 main()

