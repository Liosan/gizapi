﻿# -*- coding: utf-8 -*-

from goods import Goods
from ocupations import Ocupations, OcupationGroup
import util, logging

class ProductionReq:
	def __init__(self, ocupation, rawMaterial1=None, rawMaterial2=None): # either raw material can be None
		self.ocupation = ocupation
		self.rawMaterials = []
		if (rawMaterial1 is not None):
			self.rawMaterials.append(rawMaterial1)
		if (rawMaterial2 is not None):
			self.rawMaterials.append(rawMaterial2)

# each list element is a production option, either can be used
prodChains = {
	Goods.Food: [ProductionReq(Ocupations.Farmer), ProductionReq(Ocupations.Fisherman), ProductionReq(Ocupations.Hunter)],
	Goods.Wood: [ProductionReq(Ocupations.Woodcutter)],
	Goods.Boards: [ProductionReq(Ocupations.Carpenter, Goods.Wood)],
	Goods.Clay: [ProductionReq(Ocupations.Claydigger)],
	Goods.Bricks: [ProductionReq(Ocupations.Brickmaker, Goods.Clay, Goods.Wood)],
	Goods.Ore: [ProductionReq(Ocupations.OreMiner)],
	Goods.Iron: [ProductionReq(Ocupations.Metallurgist, Goods.Wood, Goods.Ore)],
	Goods.Tools: [ProductionReq(Ocupations.BlacksmithTools, Goods.Iron, Goods.Wood)],
	Goods.Furs: [ProductionReq(Ocupations.Hunter)],
	Goods.Clothes: [ProductionReq(Ocupations.Tailor, Goods.Furs)],
	Goods.Ceramics: [ProductionReq(Ocupations.Potter, Goods.Clay)],
	Goods.Grapes: [ProductionReq(Ocupations.GrapeFarmer)],
	Goods.Drinks: [ProductionReq(Ocupations.Winemaker, Goods.Grapes), ProductionReq(Ocupations.Brewer, Goods.Hops)],
	Goods.Salt: [ProductionReq(Ocupations.SaltGatherer)],
	Goods.Scissors: [ProductionReq(Ocupations.BlacksmithScissors, Goods.Ore, Goods.Wood)],
	Goods.Ambregris: [], # undistinguishable from basic fishermen
	Goods.Gems: [ProductionReq(Ocupations.GemMiner)],
	Goods.Horses: [ProductionReq(Ocupations.HorseBreeder)],
	Goods.Shortbows: [ProductionReq(Ocupations.BowyerShort, Goods.Wood)],
	Goods.Longbows: [ProductionReq(Ocupations.BowyerLong, Goods.Iron, Goods.Hardwood)],
	Goods.Pikes: [ProductionReq(Ocupations.WeaponsmithPikes, Goods.Wood, Goods.Iron)],
	Goods.Swords: [ProductionReq(Ocupations.WeaponsmithSwords, Goods.Wood, Goods.Iron)],
	Goods.Hops: [ProductionReq(Ocupations.HopsFarmer)],
	Goods.Hardwood: [], # undistinguishable from basic woodcutters
	Goods.Ivory: [ProductionReq(Ocupations.ElephantHunter)],
	Goods.Pianos: [ProductionReq(Ocupations.Pianomaker, Goods.Tools, Goods.Hardwood)]
}

def isGoodPresentInStorage(city, good):
	return int(city['storage'][good.code]) > 0

def isWorkerOcupationPresent(city, overviewData, ocupation):
	if ocupation.ocupationGroup == OcupationGroup.PEASANT:
		peasantsFlattened = util.readPeasantsFromCity(city)
		for peasant in peasantsFlattened:
			unitData = overviewData['units'][peasant]
			if int(unitData['idOccupation']) == ocupation.code:
				return True
	elif ocupation.ocupationGroup == OcupationGroup.TOWNSMAN:
		townsmenListFlattened = util.flattenList(city['units']['townsman'])
		for townsman in townsmenListFlattened:
			unitData = overviewData['units'][townsman]
			if int(unitData['idOccupation']) == ocupation.code:
				return True
	return False

def checkIfWorkerAndRawsPresent(prodReq, city, overviewData):
	workerPresent = isWorkerOcupationPresent(city, overviewData, prodReq.ocupation)
	allRawsPresent = True
	for raw in prodReq.rawMaterials:
		allRawsPresent = allRawsPresent and computeIsGoodAvailable(raw, city, overviewData)
	return (workerPresent, allRawsPresent)

def computeIsGoodAvailable(good, city, overviewData):
	# beautiful recursive algorithm :)
	goodPresent = False
	for prodReq in prodChains[good]:
		(workerPresent, allRawsPresent) = checkIfWorkerAndRawsPresent(prodReq, city, overviewData)
		goodPresent = goodPresent or (workerPresent and allRawsPresent)
	return goodPresent or isGoodPresentInStorage(city, good)

def computeIsProductionBlocked(good, city, overviewData):
	# beautiful recursive algorithm :)
	for prodReq in prodChains[good]:
		(workerPresent, allRawsPresent) = checkIfWorkerAndRawsPresent(prodReq, city, overviewData)
		if workerPresent and not allRawsPresent:
			return True
	return False

