﻿# -*- coding: utf-8 -*-

from google.appengine.ext import webapp
from google.appengine.ext.webapp.util import run_wsgi_app
from helpTab import HelpTab
from frame import Frame
import logging, commonTokens
from apiInterface import ApiInterface
from goods import Goods

class InventorySummary(webapp.RequestHandler):	
	def _put(self, s):
		self.response.out.write(s)

	def outputInventory(self, overviewData):		
		items = [0] * 50 # hardcoded :( but should be large enough
		for city in overviewData['cities'].values():
			storage = city['storage']
			for r in range(len(storage)):
				items[r] += storage[r]
				
		t = HelpTab(self._put, width=688)
		t.setWidths([200, 29, 200, 28, 200, 29])
		
		row = []
		imgUrl = '/img/goods/%s.png'
		for good in Goods:
			imgString = "<img src='%s'>&nbsp;%s" % (imgUrl % good.code, good.goodname)
			row.append(imgString)
			row.append("%s" % items[good.code])
			if len(row) == 6:
				t.addRow(row)
				row = []
		if len(row) > 0:
			while len(row) < 6:
				row.append("")
			t.addRow(row)
		
		t.printHTML()

	def outputProduction(self, overviewData): # TODO refactor common code with outputInventory
		items = [0] * 50 # hardcoded :( but should be large enough
		for city in overviewData['cities'].values():
			productionConsumption = city['productionConsumption']
			for r in range(len(productionConsumption)):
				items[r] += productionConsumption[r]
				
		t = HelpTab(self._put, width=688)
		t.setWidths([200, 29, 200, 28, 200, 29])
		
		row = []
		imgUrl = '/img/goods/%s.png'
		for good in Goods:
			imgString = "<img src='%s'>&nbsp;%s" % (imgUrl % good.code, good.goodname)
			row.append(imgString)
			if (items[good.code] >= 0):
				formatStr = "+%s"
			else:
				formatStr = "%s"
			row.append(formatStr % (items[good.code] / 100)) # TODO 100 should be a constant
			if len(row) == 6:
				t.addRow(row)
				row = []
		if len(row) > 0:
			while len(row) < 6:
				row.append("")
			t.addRow(row)
		
		t.printHTML()
			
	def get(self):
		self.response.headers['Content-Type'] = 'text/html'
		self._put('<html>')
		commonTokens.writeHeader(self._put, "Sumaryczny stan magazynow")
		
		self._put('<body><div style="width:1000px; margin: auto">')
		fr = Frame(self._put)
		fr.open()
		
		serverId = self.request.get('serverId', default_value='main')
		api = ApiInterface(serverId)
		
		gameId = self.request.get('gameId')
		kingdomId = self.request.get('kingdom')
		overviewData = api.getOverviewData(gameId, kingdomId, True)

		self._put(u'<b>Sumaryczny stan magazynów</b> - ' + commonTokens.countryNameWithFlag(api, overviewData['kingdom']['flag'], overviewData['kingdom']['name']))
		self._put(commonTokens.backButton('/overview?gameId=%s&kingdom=%s' % (gameId, kingdomId), api.serverId))
		self._put('<br><br>')

		self.outputInventory(overviewData)
		self._put(u'<b>Sumaryczna produkcja</b> // nie uwzględnia konsumpcji jedzenia :(<br/><br/>')
		self.outputProduction(overviewData)
	
		fr.close()
		self._put('</div>')
		self._put('</body></html>')


inventoryApplication = webapp.WSGIApplication([('/inventorySummary.*', InventorySummary)], debug=True)

def main():
	logging.getLogger().setLevel(logging.ERROR)
	run_wsgi_app(inventoryApplication)

if __name__ == "__main__":
	 main()

