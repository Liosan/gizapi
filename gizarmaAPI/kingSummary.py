﻿# -*- coding: utf-8 -*-
from google.appengine.ext import webapp
from google.appengine.ext.webapp.util import run_wsgi_app
from helpTab import HelpTab
from frame import Frame
import commonTokens, logging
from apiInterface import ApiInterface

class KingSummary(webapp.RequestHandler):
	def _put(self, s):
		self.response.out.write(s)

	def output(self, overviewData):		
		
		talentString = "<div style='background-image: url(\"/img/large/talents64.png\"); background-position: -%spx 0px;" \
			" width: 64px; height: 64px; float: left'>&nbsp;</div>" \
			"<div style='float: left;'>%s</div>";
		
		lvls = overviewData['kingdom']['king']['id']['talentLevels']
		
		t = HelpTab(self._put, width=688)
		t.setWidths([300, 44, 300, 44])
		experience = int(overviewData['kingdom']['king']['id']['expiriance']) / 12
		t.addRow([u'Poziom króla:', '%d' % int(overviewData['kingdom']['king']['id']['level']), u'Doświadczenie króla:', '%d' % experience])
		t.addRow([talentString % (7 * 64, "Charyzma"), lvls[0], talentString % (3 * 64, "Finanse"), lvls[1]])
		t.addRow([talentString % (6 * 64, u"Mądrość"), lvls[2], talentString % (9 * 64, "Architekt"), lvls[3]])
		t.addRow([talentString % (4 * 64, "Inteligencja"), lvls[8], talentString % (1 * 64, "Taktyka"), lvls[7]])
		t.printHTML()
			
	def get(self):
		self.response.headers['Content-Type'] = 'text/html'
		self._put('<html>')
		commonTokens.writeHeader(self._put, u"Status króla")
		
		self._put('<body><div style="width:1000px; margin: auto">')
		fr = Frame(self._put)
		fr.open()
		
		serverId = self.request.get('serverId', default_value='main')
		api = ApiInterface(serverId)
		
		gameId = self.request.get('gameId')
		kingdomId = self.request.get('kingdom')
		overviewData = api.getOverviewData(gameId, kingdomId, False)

		self._put(u'<b>Status króla</b> - ' + commonTokens.countryNameWithFlag(api, overviewData['kingdom']['flag'], overviewData['kingdom']['name']))
		self._put(commonTokens.backButton('/overview?gameId=%s&kingdom=%s' % (gameId, kingdomId), api.serverId))
		self.output(overviewData)
	
		fr.close()
		self._put('</div>')
		self._put('</body></html>')


kingApplication = webapp.WSGIApplication([('/kingSummary.*', KingSummary)], debug=True)

def main():
	logging.getLogger().setLevel(logging.ERROR)
	run_wsgi_app(kingApplication)

if __name__ == "__main__":
	 main()

			
			
