# -*- coding: utf-8 -*-
from goods import Goods
from overview import Overview
import util

class SupplyDemandSample:
	def __init__(self, tradeBonus):
		self.produced = {} 	# Good -> amount produced
		self.demanded = {} 	# Good -> amount demanded
		self.sold = {} 		# Good -> amount sold
		self.tradeBonus = tradeBonus

	def add(self, good, numProduced, numDemanded, numSold):
		(prevNumProduced, prevNumDemanded, prevNumSold) = self.get(good)
		self.produced[good] = prevNumProduced + numProduced
		self.demanded[good] = prevNumDemanded + numDemanded
		self.sold[good] = prevNumSold + numSold

	def get(self, good):
		numProduced = self.produced.get(good, 0)
		numDemanded = self.demanded.get(good, 0)
		numSold = self.sold.get(good, 0)
		return (numProduced, numDemanded, numSold)

class SupplyDemandInKingdom:
	def __init__(self):
		self.inTowns = {}	# cityId -> SupplyDemandSample
		self.totals = SupplyDemandSample(0.0)

def __calcProductionAndSuppliedGoods(city):
	productionConsumption = city['productionConsumption'] # difference in goods storage
	suppliedGoods = city['suppliedGoods']
	production = []
	for i in range(0, len(productionConsumption)):
		production.append(productionConsumption[i])
		if str(i) in suppliedGoods: 
			production[i] += suppliedGoods[str(i)] 
	return (production, suppliedGoods)

def __calcProdDemand(good, production, suppliedGoods, numCitizens):
	numProduced = production[good.code] / 100 # TODO 100 should be constant
	numDemanded = int(numCitizens * good.demand)
	numSold = suppliedGoods.get(str(good.code), 0)
	if not str(good.code) in suppliedGoods: # random luxury goods handling
		numDemanded = 0
	return (numProduced, numDemanded, numSold)

def __addCityAndGood(production, suppliedGoods, numCitizens, sdik, city, good):
	(numProduced, numDemanded, numSold) = __calcProdDemand(good, production, suppliedGoods, numCitizens)

	cityId = city['id']
	tradeBonus = util.getTradeBonus(int(city['tradeBonus']), numCitizens)
	citySample = sdik.inTowns.get(cityId, SupplyDemandSample(tradeBonus))
	citySample.add(good, numProduced, numDemanded, numSold)
	sdik.inTowns[cityId] = citySample

	sdik.totals.add(good, numProduced, numDemanded, numSold)	

def getSupplyDemandInKingdom(overviewData):
	sdik = SupplyDemandInKingdom()
	ov = Overview() # helper instance; TODO refactor
	for city in overviewData['cities'].values():
		(numCitizens, _, _) = ov.getNumCitizensNumUnemployedAndEliteListInCity(overviewData, city)
		(production, suppliedGoods) = __calcProductionAndSuppliedGoods(city)
		for good in Goods:
			__addCityAndGood(production, suppliedGoods, numCitizens, sdik, city, good)
	return sdik


