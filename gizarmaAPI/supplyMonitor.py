# -*- coding: utf-8 -*-
from google.appengine.ext import webapp
from google.appengine.ext.webapp.util import run_wsgi_app
import commonTokens
from apiInterface import ApiInterface
from goods import Goods
from helpTab import HelpTab
from frame import Frame
from overview import Overview
import supplyDemandCalculator

class DisplaySupply(webapp.RequestHandler):
	def _put(self, s):
		self.response.out.write(s)

	def _red(self, text):
		return "<span style='color: #87020E'>%s</span>" % text
	
	def _green(self, text):
		return "<span style='color: #1B5F00'>%s</span>" % text
	
	def _printCell(self, good, supplyDemandSample):	
		(numProduced, numDemanded, numSold) = supplyDemandSample.get(good)
		
		if (numProduced < 1) and (numDemanded < 1):
			return ""
		if numDemanded < 1: # produced only
			return self._green("+" + str(numProduced)) + "/ -"
		if numProduced < 1: # demanded only
			if numSold > 0: # but available in storage
				return u"<img src='/img/custom/storage.png' title='Brak produkcji, ale dostępne w magazynie'/> /" \
					+ str(numDemanded)
			return self._red('-') + "/" + self._red(numDemanded)
		
		# produced and demanded
		if numProduced < numDemanded: # less produced than demanded
			return self._green(numProduced) + "/" + self._red(numDemanded) 
		else:
			return self._green(numProduced) + "/" + str(numDemanded)
			
	def stringForGood(self, good):		
		imgUrl = '<img src="/img/goods/%s.png" title="%s"><br/>(%s<img width="15px" src="/img/goods/36.png">)'
		return imgUrl % (good.code, good.goodname, good.price)
	
	def _printTotalCell(self, good, supplyDemandInKingdom):
		numProduced = supplyDemandInKingdom.totals.produced.get(good, 0)
		numDemanded = supplyDemandInKingdom.totals.demanded.get(good, 0)
		return "%s/%s" % (numProduced, numDemanded)
	
	def _printProfitCell(self, good, supplyDemandInKingdom):
		numSold = supplyDemandInKingdom.totals.sold.get(good, 0)
		profit = numSold * good.price / 100  # TODO 100 should be constant
		return '%s<img width="15px" src="/img/goods/36.png">' % profit

	def get(self):
		self.response.headers['Content-Type'] = 'text/html'
		self._put('<html>')
		commonTokens.writeHeader(self._put, u"Monitor popytu i podaży")
		
		self._put('<body><div style="width:1000px; margin: auto">')
		fr = Frame(self._put)
		fr.open()

		serverId = self.request.get('serverId', default_value='main')
		api = ApiInterface(serverId)
		
		gameId = self.request.get('gameId')
		kingdomId = self.request.get('kingdom')
		overviewData = api.getOverviewData(gameId, kingdomId, True)
		supplyDemandInKingdom = supplyDemandCalculator.getSupplyDemandInKingdom(overviewData)

		self._put(commonTokens.backButton('/overview?gameId=%s&kingdom=%s' % (gameId, kingdomId), api.serverId))
		self._put('Monitor podazy - %s<br><br>' % commonTokens.countryNameWithFlag(api, overviewData['kingdom']['flag'], overviewData['kingdom']['name']))

		ct = HelpTab(self._put, width=688)
		ct.setHeaders([
			"&nbsp;Miasto&nbsp;", 
			self.stringForGood(Goods.Clothes), # TODO generalise - 2 arrays, commonGoods and luxuryGoods
			self.stringForGood(Goods.Ceramics),
			self.stringForGood(Goods.Drinks),
			self.stringForGood(Goods.Scissors),
			"",
			self.stringForGood(Goods.Salt),
			self.stringForGood(Goods.Gems),
			self.stringForGood(Goods.Ambregris),
			self.stringForGood(Goods.Ivory),
			self.stringForGood(Goods.Pianos),
			"Premia<br>kupców"])

		ov = Overview() # helper instance; TODO refactor
		for city in overviewData['cities'].values():
			(numCitizens, _, _) = ov.getNumCitizensNumUnemployedAndEliteListInCity(overviewData, city)
			cityName = city['name']
			cityId = city['id']
			supplyDemandSample = supplyDemandInKingdom.inTowns.get(cityId)
			assert supplyDemandSample is not None, "Error for cityId: %s, available towns: %s" % (cityId, str(supplyDemandInKingdom.inTowns))
			tradeBonusStr = ""
			if (supplyDemandSample.tradeBonus > 0):
				tradeBonusStr = "%0.0f%%" % (supplyDemandSample.tradeBonus * 100.0)
			ct.addRow(["%s (%s)" % (cityName, numCitizens), 
				self._printCell(Goods.Clothes, supplyDemandSample), 
				self._printCell(Goods.Ceramics, supplyDemandSample),
				self._printCell(Goods.Drinks, supplyDemandSample),
				self._printCell(Goods.Scissors, supplyDemandSample),
				"",
				self._printCell(Goods.Salt, supplyDemandSample), 
				self._printCell(Goods.Gems, supplyDemandSample),
				self._printCell(Goods.Ambregris, supplyDemandSample),
				self._printCell(Goods.Ivory, supplyDemandSample),
				self._printCell(Goods.Pianos, supplyDemandSample),
				tradeBonusStr])
		ct.addRow([""] * 11)
		ct.addRow([u"Łącznie",
				self._printTotalCell(Goods.Clothes, supplyDemandInKingdom),
				self._printTotalCell(Goods.Ceramics, supplyDemandInKingdom),
				self._printTotalCell(Goods.Drinks, supplyDemandInKingdom),
				self._printTotalCell(Goods.Scissors, supplyDemandInKingdom),
				"",
				self._printTotalCell(Goods.Salt, supplyDemandInKingdom),
				self._printTotalCell(Goods.Gems, supplyDemandInKingdom),
				self._printTotalCell(Goods.Ambregris, supplyDemandInKingdom),
				self._printTotalCell(Goods.Ivory, supplyDemandInKingdom),
				self._printTotalCell(Goods.Pianos, supplyDemandInKingdom),
				""])
		ct.addRow([u"Łączny dochód<BR>(bez kupców)",
				self._printProfitCell(Goods.Clothes, supplyDemandInKingdom),
				self._printProfitCell(Goods.Ceramics, supplyDemandInKingdom),
				self._printProfitCell(Goods.Drinks, supplyDemandInKingdom),
				self._printProfitCell(Goods.Scissors, supplyDemandInKingdom),
				"",
				self._printProfitCell(Goods.Salt, supplyDemandInKingdom),
				self._printProfitCell(Goods.Gems, supplyDemandInKingdom),
				self._printProfitCell(Goods.Ambregris, supplyDemandInKingdom),
				self._printProfitCell(Goods.Ivory, supplyDemandInKingdom),
				self._printProfitCell(Goods.Pianos, supplyDemandInKingdom),
				""])
		ct.printHTML()
		
		fr.close()
		self._put('</div>')
		self._put('</body></html>')

supplyMonitorApplication = webapp.WSGIApplication([('/displaySupply', DisplaySupply)], debug=True)

def main():
	run_wsgi_app(supplyMonitorApplication)

if __name__ == "__main__":
	 main()
