<?php

class HelpTab {
	private $headers=array();
	private $widths=array();
	private $width;
	private $data=array();
	private $noSeparator=array();
	
	public function __construct($width=false) {
		$this->width=$width;
	}
	
	public function setHeaders() {
		$tmp=&func_get_args();
		
		foreach ($tmp as &$el) {
			if (is_array($el)) {
				foreach ($el as &$el2)
					$this->headers[]=$el2;
			}
			else $this->headers[]=$el;
		}
	}
	
	public function setWidths() {
		//$this->widths=func_get_args();
		$tmp=&func_get_args();
		
		foreach ($tmp as &$el) {
			if (is_array($el)) {
				foreach ($el as &$el2)
					$this->widths[]=$el2;
			}
			else $this->widths[]=$el;
		}
	}
	
	public function addRow() {
		$row=array();
		$tmp=&func_get_args();
		
		foreach ($tmp as &$el) {
			if (is_array($el)) {
				foreach ($el as &$el2)
					$row[]=$el2;
			}
			else $row[]=$el;
		}
		
		$this->data[]=$row;
	}
	
	public function setNoSeparator() {
		$this->noSeparator[count($this->data)-1]=true;
	}
	
	public function printHTML() {
		if ($this->width) echo "<table cellspacing='0px' cellpadding='0px' border='0px' style='width:$this->width"."px'>";
		else echo "<table cellspacing='0px' cellpadding='0px' border='0px'>";
		
		echo "<tr>";
		foreach ($this->headers as $col=>$header) {
			if (isset($this->widths[$col])) {
				$w=$this->widths[$col];
				echo "<th style='width:$w;'>$header</th>";
			}
			else echo "<th>$header</th>";
		}
		echo "</tr>";
		
		$positions=array("top","center",'bottom');
		$rand=1;
		foreach ($this->data as $lp=>$row) {
			echo "<tr>";
			$i=0;
			foreach ($row as $col=>$cell) {
				
				//$rand=(4*$rand+1)%3;
				
				$rand = (($rand * 214013 + 2531011) >> 16) & 0x7fff;
				$pos=$positions[$rand%3];
				
				if ($i==0) {
					if (isset($this->noSeparator[$lp])) $tmp="";
					else $tmp="<div class='separator' style='width:$this->width"."px'></div>";
				}
				else $tmp="";
				
				if ($i%2==0) {
					if ($col==count($row)-1)
						echo "<td class='odd' style='background-image:none;'>$tmp$cell</td>";
					else
						echo "<td class='odd' style='background-position:right $pos;'>$tmp$cell</td>";
				}
				else
					echo "<td class='even' style='background-position:right $pos;'>$tmp$cell</td>";
				
				++$i;
			}
			echo "</tr>";
		}
		
		$tmp="<div class='separator' style='width:$this->width"."px'></div>";
		echo "<tr><td class='last' colspan='".count($this->headers)."'>$tmp &nbsp;</td></tr>";
		
		echo "</table>";
	}
}

?>