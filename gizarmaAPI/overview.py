﻿# -*- coding: utf-8 -*-

from google.appengine.ext import webapp
from google.appengine.ext.webapp.util import run_wsgi_app
from helpTab import HelpTab
from frame import Frame
from citizenSummary import CitizenSummary
from inventorySummary import InventorySummary
from ocupations import Ocupation, OcupationGroup, Ocupations
from goods import Goods
import commonTokens, logging, util
from apiInterface import ApiInterface
from productionChains import computeIsProductionBlocked, computeIsGoodAvailable
from buildings import Buildings

class Overview(webapp.RequestHandler):
	def _getCityHappiness(self, city):
		# 0.81 -> 0.82 temporary fix
		if 'attitude' in city:
			return float(city['attitude'])
		else:
			prosperity = float(city['prosperity'])
			return prosperity * 100.0
	
	def _put(self, s):
		self.response.out.write(s)

	def miscInfo(self, gameId, kingdomId, overviewData, rankingEntry):
		points = int(overviewData['kingdom']['points'])
		cityPoints = 0
		cityHappinessSum = 0
		cityCitizensNum = 0
		numCrosses = 0
		for city in overviewData['cities'].values():
			cityPoints += int(city['points'])
			(numCitizens, _, _) = self.getNumCitizensNumUnemployedAndEliteListInCity(overviewData, city)
			cityHappinessSum += self._getCityHappiness(city) * numCitizens
			cityCitizensNum += numCitizens
			numCrosses += int(city['crosses'])

		cityHappinessAvg = cityHappinessSum / cityCitizensNum
		cityPointsPerc = 100.0 * float(cityPoints) / float(points)

		numMilitaryUnits = 0
		totalMilitaryStrength = 0
		numUnitsWithoutOrders = 0
		for unit in overviewData['units'].values():
			ocupationId = unit['idOccupation']
			ocupation = Ocupations.get(int(ocupationId))
			assert (ocupation is not None), "Occupation %s not known" % ocupationId
			if ocupation.ocupationGroup == OcupationGroup.MILITARY:
				numMilitaryUnits += 1
				if int(unit['idSpecialty']) == ocupation.milSpecialty:
					milLevel = int(unit['level']) - ocupation.milMinLevel
				else:
					milLevel = 0
				strength = ocupation.milStrength + milLevel * ocupation.milStrengthPerLvl
				totalMilitaryStrength += strength
			elif ocupation.ocupationGroup in [OcupationGroup.MAPWORKER, OcupationGroup.SCOUT, OcupationGroup.TRANSPORT]:
				if unit['orders'] == 0:
					idGuideUnit = int(unit['idGuideUnit'])
					# no group or group guide unit
					if idGuideUnit == 0 or idGuideUnit == int(unit['id']):
						#logging.error("unit without orders: %s" % unit)
						numUnitsWithoutOrders += 1

		cash = int(overviewData['kingdom']['gold']) / 1200 # TODO '1200' should be a constant

		ct = HelpTab(self._put, width=688)
		ct.addRow(['Punkty:', points, u'% punktów z miast:', '%3.1f' % cityPointsPerc])
		ct.addRow(['Liczba miast:', len(overviewData['cities']), 'Liczba obywateli:', len(overviewData['units'])])
		ct.addRow(['Kasa:', '%s<img src="/img/goods/36.png"/>' % cash, 'Podatki:', '%s%%' % overviewData['kingdom']['tax']])
		ct.addRow([u'Średnie zadowolenie:', '%3.2f%%' % cityHappinessAvg, u'Punkty religii:', '%s<img src="/img/goods/33.png"/>' % (numCrosses / 100)])
		ct.addRow([u'Wielkość armii:', '%d' % numMilitaryUnits, u'Siła armii:', '%s<img src="/img/icon/strenght.png"/>' % totalMilitaryStrength])
		experience = int(overviewData['kingdom']['king']['id']['expiriance']) / 12
		ct.addRow([u'Poziom króla:', '%d' % int(overviewData['kingdom']['king']['id']['level']), u'Doświadczenie króla:', '%d' % experience])
		ct.addRow([u'Odbudowa wież:', '%3.2f' % float(rankingEntry['towerEffort']), u'Terytorium:', '%d' % int(rankingEntry['totalTerritory'])])
		
		ct.printHTML()
		
		if numUnitsWithoutOrders > 0:
			self._put(u'<div><img src="/img/icon/bluedot.png"/>&nbsp; Jednostki bez rozkazów: %s</div><br/>' % numUnitsWithoutOrders)
	
	def navigationBar(self, api, gameId, kingdomId):
		formatStr = '<a href="%s?gameId=%s&kingdom=%s&serverId=%s"><img src="/img/custom/%s" title="%s"/><a/><br>\n'
		self._put('<div class="navbar">')
		self._put(formatStr % ("/displaySupply", gameId, kingdomId, api.serverId, "supply-monitor.png", u"Monitor popytu i podaży"))
		self._put(formatStr % ("/financialSummary", gameId, kingdomId, api.serverId, "financial-summary.png", "Podsumowanie finansowe"))
		self._put(formatStr % ("/citizenSummary", gameId, kingdomId, api.serverId, "citizen-summary.png", "Podsumowanie obywateli"))
		self._put(formatStr % ("/inventorySummary", gameId, kingdomId, api.serverId, "inventory-summary.png", u"Sumaryczny stan magazynów"))
		self._put(formatStr % ("/buildingsSummary", gameId, kingdomId, api.serverId, "buildings.png", u"Lista budynków"))
		self._put(formatStr % ("/kingSummary", gameId, kingdomId, api.serverId, "king-status.png", u"Status króla"))
		self._put(formatStr % ("/pointsChart", gameId, kingdomId, api.serverId, "plots.png", u"Wykres punktó"))
		self._put('</div>')
	
	def getUnitOcupation(self, overviewData, unitId):
		logging.debug("printing units: %s" % overviewData['units'])
		assert (unitId in overviewData['units']), "Problem - unit %s not found in `units`; we got %s" % (unitId, overviewData['units'])
		unitData = overviewData['units'][unitId]
		occupationId = unitData['idOccupation']
		occupation = Ocupations.get(int(occupationId))
		assert (occupation is not None), "Occupation %s not known" % occupationId
		return occupation
	
	def getNumCitizensNumUnemployedAndEliteListInCity(self, overviewData, city): # TODO move to common
		townsmenListFlattened = util.flattenList(city['units']['townsman'])
		# filter out the KING
		townsmen = [t for t in townsmenListFlattened 
			if self.getUnitOcupation(overviewData, t).ocupationGroup in 
				[OcupationGroup.TOWNSMAN, OcupationGroup.ELITE]]
		numTownsmen = len(townsmen)
		
		numUnemployed = len(city['units']['unemployed'])		
		peasantsFlattened = util.readPeasantsFromCity(city)
		# filter out MAPWORKERs...
		peasants = [p for p in peasantsFlattened 
			if self.getUnitOcupation(overviewData, p).ocupationGroup == OcupationGroup.PEASANT] 
		numPeasants = len(peasants)
		
		numCitizens = numTownsmen + numUnemployed + numPeasants
			
		elite = [t for t in townsmenListFlattened 
			if self.getUnitOcupation(overviewData, t).ocupationGroup in 
				[OcupationGroup.ELITE, OcupationGroup.KING]]

		return (numCitizens, numUnemployed, elite)
	
	def getCityStatusString(self, city, overviewData, numUnemployed):
		statusString = ""
		if numUnemployed > 0:
			statusString += "<img src='/img/unit/specialty-0.png' title='Bezrobotni: %s'/><sub>%s</sub>" % (numUnemployed, numUnemployed)
		if self._getCityHappiness(city) < 30.0:
			statusString += "<img src='/img/custom/red_happiness.png' title='Niezadowolenie'/>"
		if self._getCityHappiness(city) > 70.0:
			statusString += "<img src='/img/goods/32.png' title='Zadowolenie'/>"
		if city["starvation"] > 0:
			statusString += u"<img src='/img/custom/hunger.png' title='Głód'/>"
		# storage space...
		storageSpace = int(city["storageSpace"])
		storageSpaceString = ""
		storageSpaceNumGoods = 0
		for good in Goods:
			if city["storage"][good.code] == storageSpace:
				if len(storageSpaceString) > 0:
					storageSpaceString += ", "
				storageSpaceString += good.goodname
				storageSpaceNumGoods += 1
		if len(storageSpaceString) > 0:
			statusString += u"<img src='/img/custom/storage-overflow.png' title='Pełen magazyn - %s'/><sub>%s</sub>" % (storageSpaceString, storageSpaceNumGoods)
		# production blocked...
		productionBlockedString = ""
		productionBlockedNumGoods = 0
		for good in Goods:
			if computeIsProductionBlocked(good, city, overviewData):
				if len(productionBlockedString) > 0:
					productionBlockedString += ", "
				productionBlockedString += good.goodname
				productionBlockedNumGoods += 1
		if len(productionBlockedString) > 0:
			statusString += "<img src='/img/custom/prod-fail.png' title='Blokada produkcji - %s'/><sub>%s</sub>" % (productionBlockedString, productionBlockedNumGoods)
		# construction blocked...
		constructionBlocked = False
		for (buildingInfo, towsmanList) in zip(city['buildings'], city['units']['townsman']):
			if buildingInfo['state'] == 1 and len(towsmanList) > 0:
				building = Buildings.get(buildingInfo['id'])
				if "W" in building.materials:
					constructionBlocked = constructionBlocked or not computeIsGoodAvailable(Goods.Boards, city, overviewData)
				if "B" in building.materials:
					constructionBlocked = constructionBlocked or not computeIsGoodAvailable(Goods.Bricks, city, overviewData)
				if "T" in building.materials:
					constructionBlocked = constructionBlocked or not computeIsGoodAvailable(Goods.Tools, city, overviewData)
		if constructionBlocked:
			statusString += u"<img src='/img/custom/constr-fail.png' title='Brakuje zasobów budowlanych'/>"
		return statusString
	
	def cityTable(self, overviewData):
		ct = HelpTab(self._put, width=688)
		ct.setHeaders(["&nbsp;Miasto&nbsp;", "&nbsp;Lud&nbsp;", "&nbsp;Status&nbsp;","&nbsp;Finanse&nbsp;","&nbsp;Elita&nbsp;"])
		for city in overviewData['cities'].values():
			name = city['name']
			if (city['fortressFlag'] == True):
				name += "&nbsp;<img src='/img/icon/fortified_ready.png' title='Forteca'/>"
			if (city['portFlag'] == True):
				name += "&nbsp;<img src='/img/custom/portFlag.png' title='Miasto portowe'/>"
			(numCitizens, numUnemployed, elite) = self.getNumCitizensNumUnemployedAndEliteListInCity(overviewData, city)
			
			workingElite = [e for e in elite if overviewData['units'][e]['learningFlag'] == False] # inteligencja pracujaca ;)
			learningElite = [e for e in elite if overviewData['units'][e]['learningFlag'] == True] 
			
			eliteString = ""
			for e in workingElite:
				unitOcupation = self.getUnitOcupation(overviewData, e)
				eliteString += "<img src='%s'/><sub>%s</sub>" % ("/img/unit/" + unitOcupation.image, overviewData['units'][e]['level'])
			if len(learningElite) > 0:
				eliteString += "("					
				for e in learningElite:
					unitOcupation = self.getUnitOcupation(overviewData, e)
					eliteString += "<img src='%s'/><sub>%s</sub>" % ("/img/unit/" + unitOcupation.image, overviewData['units'][e]['level'])
				eliteString += ")"
			
			financeString = "<span style='color: #1B5F00'>+%s</span>/<span style='color: #87020E'>-%s</span>" % (int(city['incom']) / 100, int(city['upkeep']) / 100)
			statusString = self.getCityStatusString(city, overviewData, numUnemployed)
			ct.addRow([name, numCitizens, statusString, financeString, eliteString])
		ct.printHTML()
	
	def selectRankingEntry(self, rankingData, kingdomId):
		for entry in rankingData:
			id = int(entry['id'])
			if id == int(kingdomId):
				return entry
		logging.error("kingdom %s not found in rankingData: %s" % (kingdomId, rankingData))
		return {}
	
	def get(self):
		self.response.headers['Content-Type'] = 'text/html'
		self._put('<html>')
		commonTokens.writeHeader(self._put, "Lista miast")
		
		self._put('<body><div style="width:1000px; margin: auto">')
		fr = Frame(self._put)
		fr.open()
		
		serverId = self.request.get('serverId', default_value='main')
		api = ApiInterface(serverId)
		
		gameId = self.request.get('gameId')
		kingdomId = self.request.get('kingdom')
		overviewData = api.getOverviewData(gameId, kingdomId, False)
		rankingData = api.getRankingData(gameId)
		rankingEntry = self.selectRankingEntry(rankingData, kingdomId)
		
		self._put(commonTokens.backButton('/ranking?gameId=%s' % gameId, api.serverId))
		self._put(commonTokens.countryNameWithFlag(api, overviewData['kingdom']['flag'], overviewData['kingdom']['name']) + "<br/><br/>\n")
		self.miscInfo(gameId, kingdomId, overviewData, rankingEntry)
		self.navigationBar(api, gameId, kingdomId)

		fr.close()
		self._put('</div><div style="width:1000px; margin: auto">')
		fr.open()

		self.cityTable(overviewData)
	
		fr.close()
		self._put('</div>')
		self._put('</body></html>')

overviewApplication = webapp.WSGIApplication([('/.*', Overview)], debug=True)

def main():
	logging.getLogger().setLevel(logging.ERROR)
	run_wsgi_app(overviewApplication)

if __name__ == "__main__":
	 main()
