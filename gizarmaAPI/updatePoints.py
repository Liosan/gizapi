﻿# -*- coding: utf-8 -*-
from google.appengine.ext import webapp, db
from google.appengine.ext.webapp.util import run_wsgi_app
from apiInterface import ApiInterface
from schema import RankingPointsSampleList, GlobalRankingSample
from time import time
import logging

class UpdatePoints(webapp.RequestHandler):	
	def _put(self, s):
		self.response.out.write(s)

	def _storeRankingPointsSampleLists(self, api, serverId):
		games = api.getGameList()
		for game in games:
			gameId = int(game['id'])
			q = RankingPointsSampleList.gql("WHERE gameId = :1 AND serverId = :2", gameId, serverId)
			
			mapByKingdomId = {}
			for entry in q:
				mapByKingdomId[entry.kingdomId] = entry;
			
			fetchedHighScores = api.getRankingData(gameId)
			for entry in fetchedHighScores:				
				kingdomId = int(entry['id'])
				points = int(entry['points'])
				
				if kingdomId not in mapByKingdomId:
					mapByKingdomId[kingdomId] = RankingPointsSampleList(gameId=gameId, serverId=serverId, kingdomId=kingdomId, pointsList=[])

				mapByKingdomId[kingdomId].pointsList.append(points)
			
			logging.info("Persisting RankingPointsSampleLists for serverId %s game %s" % (serverId, gameId))
			db.put(mapByKingdomId.values())
	
	def _storeGlobalRankingSamples(self, api, serverId):
		try:
			while True:
				q = db.GqlQuery("SELECT __key__ FROM GlobalRankingSample WHERE serverId = :serverId", serverId=serverId)
				if q.count() == 0:
					break
				db.delete(q.fetch(1000))
				time.sleep(0.5)
		except Exception, e:
			pass
		
		games = api.getGameList()
		for game in games:
			gameId = int(game['id'])
			logging.info("Persisting GlobalRankingSamples for serverId %s game %s" % (serverId, gameId))
			
			fetchedHighScores = api.getRankingData(gameId)
			for entry in fetchedHighScores:
				db.put(self._createGlobalRankingSample(api, serverId, gameId, entry))
			
	def get(self):
		t0 = time()
		serverId = self.request.get('serverId', default_value='main')
		api = ApiInterface(serverId)		
		
		self._storeRankingPointsSampleLists(api, serverId)
		self._storeGlobalRankingSamples(api, serverId)
		logging.info("Persisting samples complete after %s" % (time() - t0))
		
		
	def _createGlobalRankingSample(self, api, serverId, gameId, rankingEntry):		
		playerId = int(rankingEntry['player']['id'])
		playerName = rankingEntry['player']['name']
		if not playerName:
			playerName = "?" # case of empty player name bug - http://devforum.gizarma.pl/index.php?topic=439
		towerEffort = float(rankingEntry['towerEffort'])
		kingdomId = int(rankingEntry['id'])
		points = int(rankingEntry['points'])
		kingdomName = rankingEntry['name']
		flagId = int(rankingEntry['flag'])
		
		population = int(rankingEntry['totalPopulation'])
		cash = int(rankingEntry['gold']) / 1200 # TODO '1200' should be a constant
		ownedArea = int(rankingEntry['totalTerritory'])
		discoveredArea = int(rankingEntry['totalDiscoveredArea'])
		
		return GlobalRankingSample(serverId=serverId, playerId=playerId, kingdomId=kingdomId, gameId=gameId, playerName=playerName, kingdomName=kingdomName, flagId=flagId, 
								points=points, towerEffort=towerEffort, population=population, cash=cash, ownedArea=ownedArea, discoveredArea=discoveredArea)
		

updatePointsApplication = webapp.WSGIApplication([('/updatePoints', UpdatePoints)], debug=True)

def main():
	run_wsgi_app(updatePointsApplication)

if __name__ == "__main__":
	 main()
