﻿# -*- coding: utf-8 -*-
from google.appengine.ext import webapp
from google.appengine.ext.webapp.util import run_wsgi_app
from helpTab import HelpTab
from frame import Frame
import commonTokens
from apiInterface import ApiInterface
import logging
from schema import GlobalRankingSample

class GenericRanking(webapp.RequestHandler):	
	def _put(self, s):
		self.response.out.write(s)
	
	def _getRankingName(self, rankingId):
		if rankingId == 'points':
			return u"Ranking królestw wg punktów"
		elif rankingId == 'towerEffort': 
			return u"Ranking królestw wg odbudowy wież"
		elif rankingId == 'population':
			return u"Ranking królestw wg populacji"
		elif rankingId == 'cash':
			return u"Ranking królestw wg złota w skarbcu"
		elif rankingId == 'ownedArea':
			return u"Ranking królestw wg posiadanego terytorium"
		elif rankingId == 'discoveredArea':
			return u"Ranking królestw wg odkrytego obszaru"
		else:
			raise RuntimeError("Unknown ranking id %s" % rankingId)
	
	def _getColumnName(self, rankingId):
		if rankingId == 'points':
			return "Punkty"
		elif rankingId == 'towerEffort': 
			return "Odbudowa<br/>wież"
		elif rankingId == 'population':
			return "Populacja"
		elif rankingId == 'cash':
			return "Złoto<br/>w skarbcu"
		elif rankingId == 'ownedArea':
			return "Terytorium"
		elif rankingId == 'discoveredArea':
			return "Odkryty<br/>obszar"
		else:
			raise RuntimeError("Unknown ranking id %s" % rankingId)
	
	def _getGlobalRankingSamples(self, serverId, gameId, rankingId):
		if gameId == '':
			q = GlobalRankingSample.all().filter('serverId =', serverId).order('-%s' % rankingId)
		else:
			q = GlobalRankingSample.all().filter('serverId =', serverId).filter('gameId =', int(gameId)).order('-%s' % rankingId)
		result = q.fetch(10)
		logging.info("_getGlobalRankingSamples returning %s" % result)
		return [(elem, getattr(elem, rankingId)) for elem in result]
	
	def get(self):
		serverId = self.request.get('serverId', default_value='main')
		api = ApiInterface(serverId)
		gameId = self.request.get('gameId', default_value='')
		rankingId = self.request.get('rankingId')
		rankingName = self._getRankingName(rankingId)
		if gameId != '':
			rankingName = u"%s - gra %s" % (rankingName, gameId)
		
		self.response.headers['Content-Type'] = 'text/html'
		self._put('<html>')
		commonTokens.writeHeader(self._put, rankingName)
		
		self._put('<body><div style="width:1000px; margin: auto">')
		fr = Frame(self._put)
		fr.open()
		
		if gameId == '':
			self._put(commonTokens.backButton('/', api.serverId))
		else:
			self._put(commonTokens.backButton('/ranking?gameId=%s' % gameId, api.serverId))
		self._put('<b>%s</b><br/><small>Ranking jest uaktualniany co 24 godziny.</small><br/><br/>\n' % rankingName)
		
		
		rankingSamples = self._getGlobalRankingSamples(serverId, gameId, rankingId)
		rows = []
		for (sample, value) in rankingSamples:
			kingdomLink = '<A href="overview?gameId=%s&kingdom=%s&serverId=%s">%s</A>' % \
				(sample.gameId, sample.kingdomId, api.serverId, commonTokens.countryNameWithFlag(api, sample.flagId, sample.kingdomName))
			kingdomLinkExplicit = u'<A href="overview?gameId=%s&kingdom=%s&serverId=%s">Szczegóły >></A>' % \
				(sample.gameId, sample.kingdomId, api.serverId)
			rows.append([kingdomLink, sample.gameId, sample.playerName, value, kingdomLinkExplicit])

		rows.sort(key=lambda row: row[3], reverse=True) # sort by value
		
		scoreTable = HelpTab(self._put, width=688)
		scoreTable.setHeaders([u"Kr\u00f3lestwo", "Gra", "Gracz", self._getColumnName(rankingId), ""])
		for row in rows:
			if (row[3] > 0):
				if isinstance(row[3], float):
					row[3] = "%0.2f" % row[3]	
				scoreTable.addRow(row)
		scoreTable.printHTML()
		
		fr.close()
		self._put('</div>')
		self._put('</body></html>')

rankingApplication = webapp.WSGIApplication([('/genericRanking*', GenericRanking)], debug=True)

def main():
	logging.getLogger().setLevel(logging.ERROR)
	run_wsgi_app(rankingApplication)

if __name__ == "__main__":
	 main()
