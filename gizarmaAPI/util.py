﻿# -*- coding: utf-8 -*-

MERCHANT_BONUS = 0.5

def flattenList(l):
	return [element for sublist in l for element in sublist]

def readPeasantsFromCity(city):
	peasantsRaw = city['units']['peasants']
	if (isinstance(peasantsRaw, list)): # ok, the API is a bit... weird
		return flattenList(city['units']['peasants'])
	else:
		return flattenList(city['units']['peasants'].values())
	
def getTradeBonus(tradePower, numCitizens):		
	if (numCitizens == 0):
		return 0.0
	
	if (tradePower > numCitizens):
		tradePower = numCitizens
	
	return tradePower * MERCHANT_BONUS / numCitizens;