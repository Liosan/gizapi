﻿# -*- coding: utf-8 -*-

from string import Template
from google.appengine.ext import webapp, db
from google.appengine.ext.webapp.util import run_wsgi_app
from frame import Frame
import commonTokens
import logging
import urllib

jstpl = Template(u'swfobject.embedSWF("/ofc/open-flash-chart.swf","$title", "$width", "$height", "$flash_ver", "expressInstall.swf", {"data-file": "$data_src"});\n')
dvtpl = Template(u'<h1>$title</h1><div id="$title"></div><br/>\n')

class Chart(object):
	def __init__(self, type, name, data_src):
		self.type = ''
		self.width = 800
		self.height = 600
		self.flash_ver = '9.0.0'
		self.title = name
		self.data_src = data_src
		self.chart = None

	def get_js(self):
		return jstpl.substitute(title=self.title, width=self.width, height=self.height, flash_ver = self.flash_ver, data_src = self.data_src)
	
	def get_div(self):
		return dvtpl.substitute(title=self.title)

class PointsChart(webapp.RequestHandler):
	def _put(self, s):
		self.response.out.write(s)
 
	def add_chart(self, chart):
		self.swfobjs += chart.get_js()
		self.divs += chart.get_div()
	
	def get(self):
		self.swfobjs = ''
		self.divs = ''
	
		serverId = self.request.get('serverId', default_value='main')
		gameId = self.request.get('gameId')
		kingdomId = self.request.get('kingdom', default_value='')
		if kingdomId == '':
			paramsString = 'pointsChartData?serverId=%s&gameId=%s' % (serverId, gameId)
		else:
			paramsString = 'pointsChartData?serverId=%s&gameId=%s&kingdom=%s' % (serverId, gameId, kingdomId)
		linechart = Chart('line_chart', u'Punkty na przestrzeni dziejów', urllib.quote_plus(paramsString)) 
		self.add_chart(linechart)
		
		self.response.headers['Content-Type'] = 'text/html'
		self._put('<html>')
		commonTokens.writeHeader(self._put, u'Punkty na przestrzeni dziejów')
		
		self._put('<body><div style="width:1000px; margin: auto">')
		fr = Frame(self._put)
		fr.open()
		if kingdomId == '':
			self._put(commonTokens.backButton('/ranking?gameId=%s' % gameId, serverId))
		else:
			self._put(commonTokens.backButton('/ranking?gameId=%s&kingdom=%s' % (gameId, kingdomId), serverId))
		
		self._put('<script type="text/javascript" src="/ofc/swfobject.js"></script>')
		self._put('<script type="text/javascript">')
		self._put(self.swfobjs)
		self._put('</script>')
		self._put(self.divs)
		
		fr.close()
		self._put('</div>')
		self._put('</body></html>')

pointsChartApplication = webapp.WSGIApplication([('/pointsChart', PointsChart)], debug=True)

def main():
	logging.getLogger().setLevel(logging.DEBUG)
	run_wsgi_app(pointsChartApplication)
	
if __name__ == "__main__":
	 main()
