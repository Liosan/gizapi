﻿# -*- coding: utf-8 -*-

import types

# TODO maxCode()

class Enum:
	class __metaclass__(type):
		def __new__(upperattr_metaclass, future_class_name, future_class_parents, future_class_attr):
			cls = type.__new__(upperattr_metaclass, future_class_name, future_class_parents, future_class_attr)

			lookupDict = {}
			parseDict = {}
			for (key, val) in cls.__dict__.iteritems():
				# grab only objects that have the "code" attribute. This also filters out weird built-ins, functions etc
				if hasattr(val, 'code'):
					# insert code into lookup dict, raising error in case of conflict
					assert val.code not in lookupDict, \
						"Error while adding value %s from class %s: code %s already present in lookupDict: %s" \
						% (key, cls, val.code, lookupDict)
					lookupDict[val.code] = val

					# add a '__name' field to every object in the enum
					val.__name = key 

					# add a 'name()' method, so that is that the '__name' field can be easily accessed
					val.name = types.MethodType(lambda self: self.__name, val) 

					# if __str__ is not defined, add one that prints the name
					if not hasattr(val, '__str__'):
						val.__str__ = types.MethodType(lambda self: self.__name, val)

					# by-string lookup support
					parseDict[key] = val

			cls.__lookupDict = lookupDict
			cls.__parseDict = parseDict
	
			# inject class methods
			cls.get = classmethod(lambda self, code: self.__lookupDict.get(code))
			cls.size = classmethod(lambda self: len(self.__lookupDict))
			cls.parse = classmethod(lambda self, string: self.__parseDict.get(string))
			
			return cls
		
		def __iter__(self):
			return self.__lookupDict.values().__iter__()

#class Fruit:
#	def __init__(self, code, color, weight):
#		self.code = code
#		self.color = color
#		self.weight = weight
#	
#	def __str__(self):
#		return "Fruits." + self.name()
#	
#	def __repr__(self):
#		return self.name() + "(" + repr(self.code) + ", " + repr(self.color) + ", " + repr(self.weight) + ")"
#
#class Fruits(Enum):
#	Apple = Fruit(0, "red", 3.0)
#	Pear = Fruit(1, "green", 2.5)
#	Banana = Fruit(2, "yellow", 3.5)
#
#for f in Fruits:
#	assert f in Fruits
#	print f.name()
#	print f
#	assert Fruits.get(f.code) == f
#
#Fruits.parse('Apple')
#
#
#class TrafficLightsState:
#	def __init__(self, code, next):
#		self.code = code
#		self.next = next
#
# you can reference other enums, but only as long as they are already declared
#class TrafficLightsStates(Enum):
#	Green = TrafficLightsState(0, None)
#	Yellow = TrafficLightsState(1, Green) 
#	Red = TrafficLightsState(2, Yellow)
#	
#	# you could try to fix, but it's not pretty
#	Green.next = Red
#
#
#class ConflictingFruits(Enum):
#	Apple = Fruit(0, "red", 3.0)
#	Pear = Fruit(0, "green", 2.5) # ERROR - conflicting code
#

