def representsInt(s):
    try: 
        int(s)
        return True
    except ValueError:
        return False

class Table:
	def __init__(self, writingFunc):
		self._write = writingFunc
	
	def open(self):
		self._write("<table cellspacing='0px' cellpadding='0px' border='0px'>\n")
	
	def addHeader(self, stringList):
		self._write("<tr>")
		for s in stringList:
			self._write("<th>")
			self._write(s)
			self._write("</th>")
		self._write("</tr>\n")
	
	def addRow(self, stringList):
		odd = True
		self._write("<tr>")
		for i in range(len(stringList)):
			s = stringList[i]
			style = "" if i < len(stringList) - 1 or not odd else 'background-image: none;'
			if representsInt(s):
				style = style + ' text-align: right;'
			if style != "":
				style = 'style="' + style + '"'
			self._write('<td class="%s" %s>' % ("odd" if odd else "even", style))
			self._write(s)
			self._write('</td>')
			odd = not odd
		self._write("</tr>\n")
	
	def close(self):
		self._write("</table>\n")

