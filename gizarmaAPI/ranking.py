﻿# -*- coding: utf-8 -*-
from google.appengine.ext import webapp
from google.appengine.ext.webapp.util import run_wsgi_app
from helpTab import HelpTab
from frame import Frame
import commonTokens
from apiInterface import ApiInterface
import logging

class Ranking(webapp.RequestHandler):	
	def _put(self, s):
		self.response.out.write(s)
	
	def get(self):
		self.response.headers['Content-Type'] = 'text/html'
		self._put('<html>')
		commonTokens.writeHeader(self._put, "Ranking graczy")
		
		self._put('<body><div style="width:1000px; margin: auto">')
		fr = Frame(self._put)
		fr.open()
		
		serverId = self.request.get('serverId', default_value='main')
		api = ApiInterface(serverId)
		self._put(commonTokens.backButton('/', api.serverId))
		self._put('<b>Ranking graczy</b><br/><br/>\n')
		
		gameId = self.request.get('gameId')
		fetchedHighScores = api.getRankingData(gameId)
		rows = []
		for entry in fetchedHighScores:
			kingdomId = int(entry['id'])
			if 'idLocal' in entry:
				kingdomLocalId = entry['idLocal']
			else:
				kingdomLocalId = kingdomId 
			flagId = int(entry['flag'])
			kingdomLink = '<A href="overview?gameId=%s&kingdom=%s&serverId=%s">%s</A>' % \
				(gameId, kingdomLocalId, api.serverId, commonTokens.countryNameWithFlag(api, flagId, entry['name']))
			kingdomLinkExplicit = u'<A href="overview?gameId=%s&kingdom=%s&serverId=%s">Szczegóły >></A>' % \
				(gameId, kingdomLocalId, api.serverId)
			playerName = entry['player']['name']
			rows.append([kingdomLink, playerName, int(entry['points']), kingdomLinkExplicit])

		rows.sort(key = lambda row: row[2], reverse=True) # sort by points
		
		scoreTable = HelpTab(self._put, width=688)
		scoreTable.setHeaders([u"Kr\u00f3lestwo", "Gracz", "Punkty", ""])
		for row in rows:		
			scoreTable.addRow(row)
		scoreTable.printHTML()

		self._put('<br>')
		
		self._put(u'<a href="/pointsChart?gameId=%s&serverId=%s"><img src="/img/custom/plots.png"></a>' % (gameId, api.serverId))
		self._put(u'<a href="/genericRanking?rankingId=towerEffort&gameId=%s&serverId=%s"><img src="/img/custom/ranking-towerEffort.png"></a>' % (gameId, api.serverId))
		self._put(u'<a href="/genericRanking?rankingId=population&gameId=%s&serverId=%s"><img src="/img/custom/ranking-population.png"></a>' % (gameId, api.serverId))
		self._put(u'<a href="/genericRanking?rankingId=cash&gameId=%s&serverId=%s"><img src="/img/custom/ranking-cash.png"></a>' % (gameId, api.serverId))
		self._put(u'<a href="/genericRanking?rankingId=ownedArea&gameId=%s&serverId=%s"><img src="/img/custom/ranking-owned.png"></a>' % (gameId, api.serverId))
		self._put(u'<a href="/genericRanking?rankingId=discoveredArea&gameId=%s&serverId=%s"><img src="/img/custom/ranking-discovered.png"></a>' % (gameId, api.serverId))
		self._put('<br><br>')
		
		fetchedGames = api.getGameList()
		idWorld = -1
		for entry in fetchedGames:
			if int(gameId) == int(entry['id']):
				idWorld = int(entry['world']['id'])
		if idWorld >= 0:
			self._put(u'<div style="text-align:center">Mapa świata:<br><img src="http://alfa081.nio.pl/static/worlds/%s/start.png"></div>' % idWorld)
		
		fr.close()
		self._put('</div>')
		self._put('</body></html>')

rankingApplication = webapp.WSGIApplication([('/ranking*', Ranking)], debug=True)

def main():
	logging.getLogger().setLevel(logging.ERROR)
	run_wsgi_app(rankingApplication)

if __name__ == "__main__":
	 main()
