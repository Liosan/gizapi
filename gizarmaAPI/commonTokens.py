﻿# -*- coding: utf-8 -*-

def writeHeader(writeFunc, extraTitle):
	header = """
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Gizarmo-pomocnik - %s</title>
	<link rel="stylesheet" href="css/font.css" type="text/css"/>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	</head>
	""" % extraTitle
	writeFunc(header)

def countryNameWithFlag(apiInterface, flagId, name):
	return '<img src="%s/static/flags/%s.png"/>&nbsp;<b>%s</b>' % (apiInterface.serverUrlBase, flagId, name)

def backButton(url, serverId):
	if '?' in url:
		serverIdToken = "&serverId=%s" % serverId
	else:
		serverIdToken = "?serverId=%s" % serverId
	return '<div class="backbutton"><a href="%s%s"><img src="/img/gui/back.png"/></a></div>' % (url, serverIdToken)


