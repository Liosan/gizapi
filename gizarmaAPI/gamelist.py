﻿# -*- coding: utf-8 -*-
from google.appengine.ext import webapp
from google.appengine.ext.webapp.util import run_wsgi_app
from helpTab import HelpTab
from frame import Frame
import commonTokens
from apiInterface import ApiInterface
import logging

class GameList(webapp.RequestHandler):	
	def _put(self, s):
		self.response.out.write(s)
	
	def get(self):
		self.response.headers['Content-Type'] = 'text/html'
		self._put('<html>')
		commonTokens.writeHeader(self._put, "Gizarmo-pomocnik")
		
		self._put('<body><div style="width:1000px; margin: auto">')
		fr = Frame(self._put)
		fr.open()
		self._put('<b>Gizarmo-pomocnik - lista gier</b><br/><br/>\n')
		
		serverId = self.request.get('serverId', default_value='main')
		api = ApiInterface(serverId)
		
		fetchedGames = api.getGameList()
		rows = []
		for entry in fetchedGames:
			gameId = int(entry['id'])
			rankingLink = '<A href="ranking?gameId=%s&serverId=%s">%s</A>' % (gameId, api.serverId, gameId)
			rankingLinkExplicit = '<A href="ranking?gameId=%s&serverId=%s">Ranking >></A>' % (gameId, api.serverId)
			mapName = entry['world']['name']
			humanKingdomCount = entry['humanKingdomCount']
			rows.append([rankingLink, mapName, humanKingdomCount, rankingLinkExplicit, gameId])

		rows.sort(key = lambda row: row[4], reverse=True) # sort by newest games first
		
		scoreTable = HelpTab(self._put, width=688)
		scoreTable.setHeaders([u"Numer gry", "Mapa", "Gracze", ""])
		for row in rows:		
			scoreTable.addRow([row[0], row[1], row[2], row[3]])
		scoreTable.printHTML()
		
		self._put(u'<a href="/genericRanking?rankingId=points&serverId=%s"><img src="/img/custom/plots.png"></a>' % (api.serverId))
		self._put(u'<a href="/genericRanking?rankingId=towerEffort&serverId=%s"><img src="/img/custom/ranking-towerEffort.png"></a>' % (api.serverId))
		self._put(u'<a href="/genericRanking?rankingId=population&serverId=%s"><img src="/img/custom/ranking-population.png"></a>' % (api.serverId))
		self._put(u'<a href="/genericRanking?rankingId=cash&serverId=%s"><img src="/img/custom/ranking-cash.png"></a>' % (api.serverId))
		self._put(u'<a href="/genericRanking?rankingId=ownedArea&serverId=%s"><img src="/img/custom/ranking-owned.png"></a>' % (api.serverId))
		self._put(u'<a href="/genericRanking?rankingId=discoveredArea&serverId=%s"><img src="/img/custom/ranking-discovered.png"></a>' % (api.serverId))
		self._put('<br><br>')
	
		fr.close()		
		self._put('</div>')
		self._put('</body></html>')

rankingApplication = webapp.WSGIApplication([('/.*', GameList)], debug=True)

def main():
	logging.getLogger().setLevel(logging.ERROR)
	run_wsgi_app(rankingApplication)

if __name__ == "__main__":
	 main()
