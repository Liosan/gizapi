﻿# -*- coding: utf-8 -*-

import urllib2
import json

class ApiInterface(object):	
	serverUrlBase = ""
	apiUrlBase = ""
	idFixingMode = ""
	serverId = ""

	def __init__(self, serverIdToInitialize):	
		self.serverId = serverIdToInitialize
		if self.serverId == "ovh":
			self.serverUrlBase = 'http://vps111849.ovh.net/gizarma082-develop/webStarter/www/'
			self.apiUrlBase = 'http://vps111849.ovh.net/gizarma082-develop/server/www/open-api.php'
			self.idFixingMode = 'to-int'
		elif self.serverId == "ovh-master":
			self.serverUrlBase = 'http://vps111849.ovh.net/gizarma082-master/webStarter/www/'
			self.apiUrlBase = 'http://vps111849.ovh.net/gizarma082-master/server/www/open-api.php'
			self.idFixingMode = 'to-int'
		elif self.serverId == "main":
			self.serverUrlBase = 'http://alfa081.nio.pl/'
			self.apiUrlBase = 'http://alfa081s.nio.pl/open-api.php' # note 's' in domain
			self.idFixingMode = 'to-int'
		elif self.serverId == "local":
			self.serverUrlBase = 'http://localhost/webStarter/www/'
			self.apiUrlBase = 'http://localhost/server/www/open-api.php'
			self.idFixingMode = 'to-int'
		
	def _fetch(self, url):
		contents = urllib2.urlopen(url, None, 30).read()
		return json.loads(contents)
	
	def getOverviewData(self, gameId, kingdomId, enhanced):
		if self.serverId == "":
			raise "ServerId not initialized!"
		if enhanced:
			overviewAction = "kingdom_state_enhanced"
		else:
			overviewAction = "kingdom_state_limited"
		url = (self.apiUrlBase + '?action=%s&id_game=%s&id_kingdom=%s') % (overviewAction, gameId, kingdomId)
		overviewData = self._fetch(url)
		unitsDict = {}
		for key in overviewData['units']:
			if self.idFixingMode == 'to-int':
				unitsDict[int(key)] = overviewData['units'][key]
			elif self.idFixingMode == 'to-string':
				unitsDict[str(int(key))] = overviewData['units'][key] 
		overviewData['units'] = unitsDict
		return overviewData
	
	def getRankingData(self, gameId):
		if self.serverId == "":
			raise "ServerId not initialized!"
		url = (self.apiUrlBase + "?action=ranking&id_game=%s") % gameId
		return self._fetch(url)
	
	def getGameList(self):
		if self.serverId == "":
			raise "ServerId not initialized!"
		return self._fetch(self.apiUrlBase + "?action=game_list")


