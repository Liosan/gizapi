﻿# -*- coding: utf-8 -*-
from google.appengine.ext import webapp
from google.appengine.ext.webapp.util import run_wsgi_app
from helpTab import HelpTab
from frame import Frame
from ocupations import Ocupation, OcupationGroup, Ocupations
from collections import defaultdict
import commonTokens, logging
from apiInterface import ApiInterface

class CitizenSummary(webapp.RequestHandler):
	def _put(self, s):
		self.response.out.write(s)

	def makeSection(self, name, overviewData, ocupationGroupList, isLearning):
		self._put('<p class="highlight">%s</p>' % name)
		
		d = defaultdict(int)
		for unit in overviewData['units'].values():
			ocupationId = unit['idOccupation']
			ocupation = Ocupations.get(int(ocupationId))
			if ocupation.ocupationGroup in ocupationGroupList:
				if unit['learningFlag'] == isLearning:
					d[ocupation.code] += 1
		
		pairsList = [] # (string, count)
		for item in d:
			ocupation = Ocupations.get(item)
			s = "<img src='/img/unit/%s'>%s:" % (ocupation.image, ocupation.unitname)
			pairsList.append((s, d[item]))
		
		if len(pairsList) == 0:
			self._put("<span style='color: #1B5F00'>BRAK</span>")
		else:
			oddList = pairsList[1::2]
			evenList = pairsList[::2]
		
			t = HelpTab(self._put, width=688)
			t.setWidths([300, 44, 300, 44])

			for i in range(len(evenList)):
				t.addRow([evenList[i][0], evenList[i][1], "" if i == len(oddList) else oddList[i][0], "" if i == len(oddList) else oddList[i][1]])
		
			t.printHTML()
	
	def output(self, overviewData):		
		self.makeSection(u"Chłopi", overviewData, [OcupationGroup.PEASANT], False)
		self.makeSection(u"Rzemieślnicy", overviewData, [OcupationGroup.TOWNSMAN], False)
		self.makeSection("Elita", overviewData, [OcupationGroup.KING, OcupationGroup.ELITE], False)
		self.makeSection("Studenci", overviewData, [OcupationGroup.KING, OcupationGroup.ELITE], True)
		self.makeSection("Jednostki cywilne", overviewData, [OcupationGroup.MAPWORKER, OcupationGroup.SCOUT, OcupationGroup.TRANSPORT], False)
		self.makeSection("Jednostki wojskowe", overviewData, [OcupationGroup.MILITARY], False)
			
	def get(self):
		self.response.headers['Content-Type'] = 'text/html'
		self._put('<html>')
		commonTokens.writeHeader(self._put, "Podsumowanie obywateli")
		
		self._put('<body><div style="width:1000px; margin: auto">')
		fr = Frame(self._put)
		fr.open()
		
		serverId = self.request.get('serverId', default_value='main')
		api = ApiInterface(serverId)
		
		gameId = self.request.get('gameId')
		kingdomId = self.request.get('kingdom')
		overviewData = api.getOverviewData(gameId, kingdomId, False)

		self._put(u'<b>Podsumowanie obywateli (wg zajęcia)</b> - ' + commonTokens.countryNameWithFlag(api, overviewData['kingdom']['flag'], overviewData['kingdom']['name']))
		self._put(commonTokens.backButton('/overview?gameId=%s&kingdom=%s' % (gameId, kingdomId), api.serverId))

		self.output(overviewData)
	
		fr.close()
		self._put('</div>')
		self._put('</body></html>')


citizenApplication = webapp.WSGIApplication([('/citizenSummary.*', CitizenSummary)], debug=True)

def main():
	logging.getLogger().setLevel(logging.ERROR)
	run_wsgi_app(citizenApplication)

if __name__ == "__main__":
	 main()

			
			
