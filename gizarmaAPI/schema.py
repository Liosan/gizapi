﻿# -*- coding: utf-8 -*-

from google.appengine.ext import db

class RankingPointsSampleList(db.Model):
	gameId = db.IntegerProperty(required=True)
	serverId = db.StringProperty(required=True)
	kingdomId = db.IntegerProperty(required=True)
	pointsList = db.ListProperty(long, required=True)

class GlobalRankingSample(db.Model):
	serverId = db.StringProperty(required=True)
	playerId = db.IntegerProperty(required=True)
	kingdomId = db.IntegerProperty(required=True)
	gameId = db.IntegerProperty(required=True)
	
	playerName = db.StringProperty(required=True)
	kingdomName = db.StringProperty(required=True)
	flagId = db.IntegerProperty(required=True)
	
	points = db.IntegerProperty(required=True)
	towerEffort = db.FloatProperty(required=True)
	population = db.IntegerProperty(required=True)
	cash = db.IntegerProperty(required=True)
	ownedArea = db.IntegerProperty(required=True)
	discoveredArea = db.IntegerProperty(required=True)