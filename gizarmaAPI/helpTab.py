class HelpTab:
	def __init__(self, writingFunc, width = 0):
		self._write = writingFunc	
		self._width = width

		self._headers = []
		self._widths = [];
		self._data = [];
		self._noSeparator = []
		
	def setHeaders(self, headersList):
		self._headers = headersList
	
	def setWidths(self, widthList):
		self._widths = widthList
	
	def addRow(self, stringList):
		self._data.append(stringList)
		self._noSeparator.append(False)
	
	def setNoSeparator(self):
		self._noSeparator[-1] = True
	
	def printHTML(self):
		if (self._width > 0):
			self._write("<table cellspacing='0px' cellpadding='0px' border='0px' style='width: %spx'>" % self._width)
		else:
			self._write("<table cellspacing='0px' cellpadding='0px' border='0px'>")
		
		self._write("<tr>")
		for col in range(len(self._headers)):
			header = self._headers[col]
			if (len(self._widths) > col and self._widths[col] > 0):
				w = self._widths[col]
				self._write("<th style='width: %s;'>%s</th>" % (w, header))
			else:
				self._write("<th>%s</th>" % header)
		
		self._write("</tr>")
		
		positions = ["top","center",'bottom']
		rand = 1
		for lp in range(len(self._data)):
			row = self._data[lp]
			self._write("<tr>")
			i = 0
			for col in range(len(row)):
				cell = row[col]
				rand = ((rand * 214013 + 2531011) >> 16) & 0x7fff
				pos = positions[rand % 3]
				
				if (i == 0):
					if (self._noSeparator[lp]):
						tmp = ""
					else:
						tmp = "<div class='separator' style='width:%spx'></div>" % self._width
				else:
					tmp = ""
				
				widthString = ""
				if len(self._headers) == 0:
					if (len(self._widths) > col and self._widths[col] > 0):
						widthString = " width: %spx;" % self._widths[col]
				
				if (i % 2 == 0):
					if (col == len(row) - 1):
						self._write("<td class='odd' style='background-image:none; %s'>%s%s</td>" % (widthString, tmp, cell))
					else:
						self._write("<td class='odd' style='background-position:right %s; %s'>%s%s</td>" % (pos, widthString, tmp, cell))
				else:
					self._write("<td class='even' style='background-position:right %s; %s'>%s%s</td>" % (pos, widthString, tmp, cell))
				
				i += 1
			
			self._write("</tr>")
		
		tmp = "<div class='separator' style='width:%spx'></div>" % self._width
		self._write("<tr><td class='last' colspan='%s'>%s &nbsp;</td></tr>" % (len(self._headers), tmp))
		
		self._write("</table>")


